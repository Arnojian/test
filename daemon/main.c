#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int daemon_init(void)
{
	pid_t pid ;
	if((pid = fork())<0)
	{
		printf("create child process is error\n") ;
		return -1 ;
	}
	else if(0!=pid)
	{
		exit(0) ;
	}
	else
	{
		setsid() ;// become session leader
		chdir("/") ; // change working deirectory
		umask(0) ;// clear our file mode creation mask
	}
	return 0 ;
}

int main()
{
	daemon_init() ;
	sleep(20) ;
	return 0 ;
}
