#include<stdio.h>
#include<stdint.h>
#include<sys/types.h>
#include<string.h>
#include<malloc.h>
#include"test.pb-c.h" 

#define BUFF_SIZE 1024
int main()
{
	LogonReqMessage req =LOGON_REQ_MESSAGE__INIT ;
	LogonReqMessage *pReq = NULL ;
	int packSize = 0 ,res = 0 ;
	uint8_t *tmp = malloc(BUFF_SIZE) ;
	req.has_acctid = 1 ;
	req.acctid = 1 ;
	req.passwd = malloc(1024) ;
	memset(req.passwd,0,1024);
	strcpy(req.passwd,"luojian") ;
	
	req.has_space = 1;
	req.space = 123 ;

	packSize = logon_req_message__get_packed_size(&req) ;
	printf("the length of req message :%d \n",packSize) ;
	
	printf("pack the req message\n") ;
	res = logon_req_message__pack(&req,tmp) ;
		
	printf("unpack the message \n") ;
	pReq =(LogonReqMessage*) logon_req_message__unpack(NULL,packSize,tmp) ;
	

	printf("unpack the message, pReq->accId:%d,passwd:%s\n",pReq->acctid,pReq->passwd) ;

	logon_req_message__free_unpacked(pReq,NULL) ;
	
	pReq = NULL ;

	return 0 ;
}
