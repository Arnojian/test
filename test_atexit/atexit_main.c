#include <stdio.h>
#include <stdlib.h>

static void myExit1(void);
static void myExit2(void);

int main(void)
{
	
	if ( atexit(myExit2)!= 0)
	{
		printf("atexit myExit2 is error\n") ;
		return 1 ;
	}
	if( atexit(myExit1)!=0)
	{
		printf("atexit myExit1 is error\n") ;
		return 1 ;
	}
	if( atexit(myExit1)!=0)
	{
		printf("atexit myExit12 is error\n") ;
		return 1 ;
	}
	printf("main is done\n") ;

	return 0 ;
}

static void myExit1()
{
	printf("this is myExit1\n") ;
}
static void myExit2()
{
	printf("this is myExit2\n") ;
}
