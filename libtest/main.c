#include<stdio.h>
#include<dlfcn.h>
#include"libtest.h"
#define DLL_FNC_NAME "liblibtest.so"
int main()

{
	int (*fcn)(int,int) ;
	void *handle =NULL;
	if((handle=dlopen(DLL_FNC_NAME,RTLD_LAZY))==NULL)
	{
		printf("dfopen is error :%s\n",dlerror()) ;
		return -1 ;
	}
	
	fcn = dlsym(handle,"add") ;
	printf("1+3=%d\n",fcn(1,3)) ;	
	fcn = dlsym(handle,"sub") ;
	printf("1-3=%d\n",fcn(1,3)) ;
	dlclose(handle) ;
	return 0 ;
}
