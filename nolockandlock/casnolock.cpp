#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

#include "timer.h"

int mutex = 0 ;
int lock = 0 ;
int unlock = 1 ;

static volatile int count = 0 ;

void * testFunc(void *arg) 
{
	int i = 0 ;
	for(i = 0 ;i<2000000; i++)
	{
		while(!__sync_bool_compare_and_swap(&mutex,lock,1))
			usleep(100000) ;
		count ++ ;
		while(!__sync_bool_compare_and_swap(&mutex,unlock,0)) 
			usleep(100000) ;
	}
	pthread_exit(0) ;
}

int main(int argc,char *argv[])
{
	Timer timer ;
	timer.start() ;
	pthread_t threadId[10] ;

	int i = 0 ;
	for(i = 0 ;i< sizeof(threadId)/sizeof(pthread_t);i++)
	{
		pthread_create(&threadId[i],NULL,testFunc,NULL) ;
	}

	for(i = 0 ;i< sizeof(threadId)/sizeof(pthread_t);i++)
	{
		pthread_join(threadId[i],NULL) ;
	}
	timer.stop();
	timer.costTime() ;
	printf("count:%d\n",count) ;
	return 0 ;
}

