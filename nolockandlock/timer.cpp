#include <stdio.h>
#include "timer.h"

Timer::Timer()
{
	begin = false ;
	end = false ;
}

void Timer::start()
{
	begin = true ;
	gettimeofday(&timeStart,NULL) ;
}

void Timer::stop()
{
	if(begin == true)
	{
		gettimeofday(&timeEnd,NULL);
		end=true ;
	}
}

void Timer::reset()
{
	begin= false ;
	end = false ;
}

void Timer::costTime()
{
	if(begin==false)
	{
		printf("error count time,you need to exec start() at first\n") ;
		return ;
	}
	else if(end == false)
	{
		printf("you need stop timer \n") ;
		return ;
	}
	else
	{
		int usec = 0,sec = 0;
		bool borrow = false ;
		if(timeEnd.tv_usec > timeStart.tv_usec)
		{
			usec = timeEnd.tv_usec - timeStart.tv_usec ;
		}
		else
		{
			borrow = true ;
			usec = timeEnd.tv_usec + 1000000 - timeStart.tv_usec ;
		}

		if(borrow == false)
		{
			sec = timeEnd.tv_sec - timeStart.tv_sec ;
		}
		else
		{
			sec = timeEnd.tv_sec -1 - timeStart.tv_sec ;
		}

		printf("cost time is sec:%d usec:%d\n",sec,usec) ;
	}
}





