#ifndef _TIMER_H
#define _TIMER_H

#include <sys/time.h>

class Timer{
	public:
		Timer() ;

		void start() ;
		void stop() ;
		void reset() ;
		void costTime() ;
	private:
		struct timeval timeStart ;
		struct timeval timeEnd ;
		bool begin,end;
};

#endif
