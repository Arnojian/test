#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#include "timer.h"

pthread_mutex_t gMutex ;
static volatile int count = 0 ;

void * testFunc(void *arg)
{
	int i = 0 ;
	for(i = 0 ;i<2000000; i++)
	{
		pthread_mutex_lock(&gMutex) ;
		count++;
		pthread_mutex_unlock(&gMutex) ;
	}
	pthread_exit(0) ;
}

int main(int argc,char *argv[])
{
	Timer timer ;
	pthread_t threadId[10] ;

	timer.start() ;
	pthread_mutex_init(&gMutex,NULL) ;
	
	int i = 0 ;
	for(i = 0 ;i< sizeof(threadId)/sizeof(pthread_t);i++)
	{
		pthread_create(&threadId[i],NULL,testFunc,NULL) ;
	}

	for(i = 0 ;i< sizeof(threadId)/sizeof(pthread_t) ;i++)
	{
		pthread_join(threadId[i],NULL) ;
	}

	timer.stop() ;
	timer.costTime() ;
	printf("count:%d\n",count) ;
	return 0 ;
	pthread_mutex_destroy(&gMutex) ;
}

