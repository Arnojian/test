#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

#include "timer.h"

static volatile int count = 0 ;

void * testFunc(void *arg)
{
	int i =  0;
	for(i = 0 ;i< 2000000;i++)
	{
		count++ ;
	}
	pthread_exit(0) ;
}

int main(int argc,char *argv[])
{
	Timer timer ;
	timer.start() ;

	pthread_t threadId[10] ;
	int i = 0 ;
	for(i = 0 ;i< sizeof(threadId)/sizeof(pthread_t) ;i++)
	{
		pthread_create(&threadId[i],NULL,testFunc,NULL) ;
	}

	for(i = 0 ;i< sizeof(threadId)/sizeof(pthread_t);i++)
	{
		pthread_join(threadId[i],NULL) ;
	}
	
	timer.stop() ;
	timer.costTime() ;
	printf("count:%d\n",count) ;
	return 0 ;
}
