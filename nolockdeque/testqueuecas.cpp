#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

#include "queuecas.h"
int threadCount ;
Stack<int> stack ;
void *testFunc(void *arg)
{
	
	int i = 0 ;
	printf("pid:%d\n",pthread_self());
	for(i = 0 ;i<100 ; i++)
	{
	//	printf("pid:%d,i:%d\n",getpid(),i);
		stack.push(i) ;
	}
	
	pthread_exit(0) ;
}

void *testFuncTwo(void *arg)
{
	int count = 0 ;
	printf("consume pid:%d\n",pthread_self()) ;
	while(1)
	{
		if(stack.isEmpty())
			continue;
		stack.pop();
		count++ ;
		if(count==threadCount*100)
			break ;
	}
	pthread_exit(0) ;
}

int main(int argc,char *argv[])
{
	if(argc < 2)
	{
		printf("input the parameters\n") ;
		return 0 ;
	}

	threadCount = atoi(argv[1]) ;
	int i = 0 ;
	pthread_t threadIds[threadCount],thread2 ;
	for(i = 0 ;i<threadCount; i++)
	{
		pthread_create(&threadIds[i],NULL,testFunc,NULL) ;
	}
	pthread_create(&thread2,NULL,testFuncTwo,NULL);

	for(i = 0 ;i<threadCount ;i++)
	{
		pthread_join(threadIds[i],NULL);
	}
	pthread_join(thread2,NULL);
	
	printf("main is exit\n") ;

	return 0 ;
}
