#ifndef _QUEUE_WITH_MUTEX_HPP
#define _QUEUE_WITH_MUTEX_HPP

#include <stdio.h>

template <typename T>
class Node{
	public :
		T data ;
		Node *next ;
		Node(T& data);
		~Node();
};

template <typename T>
Node<T>::Node(T& data):data(data),next(NULL){}

template <typename T>
Node<T>::~Node(){}

template <typename T>
class Stack{
	private:
		Node<T> *top ;
	public:
		Stack():top(NULL){}
		void push(T& data) ;
		T pop() ;
		bool isEmpty() ;
};

template<typename T>
bool Stack<T>::isEmpty()
{
	if(NULL == top)
		return true ;
	return false;
}
template<typename T>
void Stack<T>::push(T& data)
{
	Node<T> *node = new Node<T>(data) ;
	
	while(1)
	{
		node->next = top ;
		if(__sync_bool_compare_and_swap(&top,node->next,node)) // atomic operation
		{
			printf("push :%d\n",data);
			break ;
		}
	}

	return ;
}

template<typename T>
T Stack<T>::pop()
{
	T t ;
	Node<T> *node = NULL,*tmp= NULL ;
	while(1)
	{
		node = top->next ;
		tmp = top ;
		if(__sync_bool_compare_and_swap(&top,tmp,node))
		{
			break ;
		}
	}
	printf("pop:%d\n",tmp->data);
	t = tmp->data ;
	delete tmp ;
	return t ;
}

#endif
