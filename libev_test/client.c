#include <stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>

#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>

#define IP "127.0.0.1"
#define PORT 8888

int main(int argc, char *argv[])
{
	
	int fd = -1 ;
	struct sockaddr_in servAddr ;
	char buff[100] = "luojian" ;
	int sendLen ;
	
	printf("libev client\n");
	memset(&servAddr,0x00,sizeof(servAddr)) ;
	servAddr.sin_family = AF_INET ;
	servAddr.sin_addr.s_addr = inet_addr(IP) ;
	servAddr.sin_port = htons(PORT) ;

	fd = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP) ;
	if(fd < 0)
	{
		printf("create socket is error:%s",strerror(errno)) ;
		exit(-1) ;
	}
	
	if(connect(fd,(struct sockaddr*)&servAddr,sizeof(servAddr))<0)
	{
		printf("connect is failed :%s\n",strerror(errno)) ;
		return -1 ;
	}
	printf("connect is ok\n") ;
	
	sendLen=send(fd,buff,8,0) ;
	if(sendLen < 0)
	{
		printf("send is error:%s\n",strerror(errno)) ;
		return -1 ;
	}
	
	printf("send is ok\n") ;

	int recvLen = recv(fd,buff,100,0) ;
	if(recvLen == -1)
	{
		printf("recv is error:%s\n",strerror(errno)) ;
		return -1 ;
	}
	else if(recvLen ==0)
	{
		printf("recv socket is close by peer\n") ;
		close(fd) ;
		return 0 ;
	}
	else
	{
		buff[recvLen] = '\0';
		printf("recv data len:%d,data:%s\n",recvLen,buff) ;
	}

	printf("recv is ok");
	close(fd) ;
	return 0;
}
