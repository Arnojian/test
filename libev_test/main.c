#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<errno.h>
#include<string.h>

#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>

#include<ev.h>


#define IP "127.0.0.1"
#define PORT 8888

int openConnection(char *ip,int port)
{
	int fd ;
	struct sockaddr_in servAddr ;
	int optVal = 1024 ;

	memset(&servAddr,0x00,sizeof(servAddr)) ;
	servAddr.sin_family=AF_INET ;
	servAddr.sin_port=htons(port) ;
	servAddr.sin_addr.s_addr = inet_addr(ip) ;

	if((fd = socket(AF_INET,SOCK_STREAM,0)) < 0)
	{
		printf("error create socket :%s",strerror(errno)) ;
		return -1 ;
	}
	
	if(bind(fd,(struct sockaddr*)&servAddr,sizeof(servAddr))<0)
	{
		printf("bind the socket is error:%s",strerror(errno)) ;
		close(fd) ;
		return -1 ;
	}

	if(setsockopt(fd,SOL_SOCKET,SO_SNDBUF,&optVal,sizeof(optVal))<0)
	{
		printf("setsockopt SO_SNDBUF is err:%s",strerror(errno)) ;
		close(fd);
		return -1 ;
	}

	if(setsockopt(fd,SOL_SOCKET,SO_RCVBUF,(const char *)&optVal,sizeof(optVal))<0)
	{
		printf("setsockopt SO_RCVBUF is err:%s",strerror(errno)) ;
		close(fd) ;
		return -1 ;
	}

	optVal = 1;
	if(setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,(const char *)&optVal,sizeof(optVal))<0)
	{
		printf("setsockopt SO_REUSERADDR is err:%s\n",strerror(errno)) ;
		close(fd) ;
		return -1 ;
	}

	if(listen(fd,5)<0)
	{
		printf("listen is error:%s\n",strerror(errno)) ;
		close(fd) ;
		return -1 ;
	}

	printf("open connection is ok,%s:%d\n",ip,port);

	return fd ;
}

void handleRecvData(struct ev_loop *loop,struct ev_io *watcher,int revents)
{
	int recvLen = -1 ;
	char buff[100] ;
	recvLen = recv(watcher->fd,buff,100,0) ;
	if(recvLen <0)
	{
		printf("client recv is error:%s",strerror(errno)) ;
		ev_io_stop(loop,watcher);
		close(watcher->fd) ;
		free(watcher);
		return ;
	}
	else if(recvLen ==0)
	{
		printf("client is close") ;
		ev_io_stop(loop,watcher);
		close(watcher->fd) ;
		free(watcher);
		return ;
	}

	printf("recv :%s\n",buff) ;
	send(watcher->fd,buff,100,0) ;
	printf("send is ok\n") ;
	return ;
}

void acceptSocket(struct ev_loop *loop,struct ev_io *watcher,int revents)
{
	printf("acceptSocket begin") ;

	struct sockaddr_in cliAddr ;
	socklen_t sockLen ;
	int cliFd = -1 ;
	
	struct ev_io *cliEvIO = (struct ev_io*)malloc(sizeof(struct ev_io)) ;
	memset(cliEvIO,0x00,sizeof(struct ev_io)) ;

	memset(&cliAddr,0x00,sizeof(cliAddr)) ;

	cliFd = accept(watcher->fd,(struct sockaddr*)&cliAddr,&sockLen)  ;
	if(cliFd<0)
	{
		printf("accept is failed:%s\n",strerror(errno)) ;
		return ;
	}

	ev_io_init(cliEvIO,handleRecvData,cliFd,EV_READ) ;
	ev_io_start(loop,cliEvIO) ;

	printf("accept the client %s:%d",inet_ntoa(cliAddr.sin_addr),ntohs(cliAddr.sin_port)) ;

	return ;
}

int main()
{
	int fd = -1 ;
	struct ev_loop *loop = ev_default_loop(0) ;
	struct ev_io evIO ;
	fd=openConnection(IP,PORT) ;

	if(-1 == fd)
	{
		exit(-1) ;
	}
	
	ev_io_init(&evIO,acceptSocket,fd,EV_READ) ;
	ev_io_start(loop,&evIO) ;
	ev_loop(loop,0) ;

	return 0 ;
}