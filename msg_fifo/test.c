#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include "msg_fifo.h"

static int runtimes = 0 ;

int freeData(void *data,const char* file,int line)
{
	free(data) ;
	return 0 ;
}

MsgFifo_t *pgFifo = NULL;

void *testFuncPush()
{
	int i = 0 ;
	char *p = NULL ;
	while(i<runtimes)
	{
		p = (char*)malloc(100) ;
		memset(p,'L',99) ;
		p[99]='\0' ;
		msgFifoPush(pgFifo,(void *)p);
		i++ ;
	}
	return NULL;
}

void *testFunPop()
{
	char *p = NULL ;
	while(msgFifoGetCurNum(pgFifo)!=0)
	{
		p = (char*)msgFifoPop(pgFifo) ;
		printf("%s\n",p) ;
		pgFifo->freeNodeFunc(p,__FILE__,__LINE__) ;
	}
	return NULL;
}

int main(int argc,char *argv[])
{	

	if(argc <2)
	{
		printf("please input the parameters\n");
		return 0;
	}
	
	runtimes = atoi(argv[1]);

	pgFifo = msgFifoInit(200,freeData);
	
	testFuncPush() ;
	printf("total:%d\n",msgFifoGetCurNum(pgFifo) ) ;
	testFunPop() ;
	msgFifoFree(pgFifo) ;
	return 0 ;
}




