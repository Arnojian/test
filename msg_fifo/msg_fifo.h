#ifndef _MSG_FIFO_H
#define _MSG_FIFO_H

#ifdef __cplusplus
extern "C" {
#endif

typedef int (*freeNode_f)(void *,const char *file,int line);

typedef struct msgNode{
	struct msgNode * next ;
	void * data ;
}MsgNode_t ;

typedef struct msgFifo{
	
	MsgNode_t *head ;
	MsgNode_t *tail ;
	MsgNode_t *freeNodeList ;
	char *pmalloc ;
	int nodesNum ; // used
	int maxNum ;  // max
	freeNode_f freeNodeFunc ;

}MsgFifo_t ;

MsgFifo_t *msgFifoInit(int maxNodes,freeNode_f freeNodeFunc) ;

int msgFifoPush(MsgFifo_t *pFifo,void *data) ;

void *msgFifoPop(MsgFifo_t *pFifo);

void *msgFifoPeek(MsgFifo_t *pFifo);

void msgFifoFree(MsgFifo_t *pFifo) ;

int msgFifoGetCurNum(MsgFifo_t *pFifo);

#ifdef __cplusplus 
}
#endif

#endif
