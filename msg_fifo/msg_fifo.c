#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include "msg_fifo.h"

static int gCount = 0 ;

MsgFifo_t *msgFifoInit(int maxNodes,freeNode_f freeNodeFunc)
{
	MsgFifo_t *pFifo = NULL ;
	MsgNode_t *p = NULL ;
	int i = 0 ;

	pFifo = (MsgFifo_t*)malloc(sizeof(MsgFifo_t)) ;
	if(NULL == pFifo)
	{
		return NULL ;
	}

	memset(pFifo,0x00,sizeof(MsgFifo_t));

	pFifo->pmalloc = (char *)malloc(maxNodes*sizeof(MsgNode_t)) ;
	if(NULL == pFifo->pmalloc)
	{
		free(pFifo) ;
		return NULL ;
	}

	// init the freeNodeList
	memset(pFifo->pmalloc,0x00,sizeof(MsgNode_t)*maxNodes) ;
	pFifo->freeNodeList =(MsgNode_t*)pFifo->pmalloc ;
	p = pFifo->freeNodeList;
	for(i = 0 ;i<maxNodes;i++)
	{
		p->next = p+1 ;
		p++;
	}
	p->next = NULL ;

	pFifo->nodesNum = 0 ;
	pFifo->maxNum = maxNodes;
	pFifo->freeNodeFunc = freeNodeFunc ;

	return pFifo ;
}

MsgNode_t *msgFifoNodeAlloc(MsgFifo_t *pFifo)
{
	MsgNode_t *node = NULL;
	if(!pFifo)
		return NULL;
	
	if(!pFifo->freeNodeList)
		return NULL;
	else
	{
		node = pFifo->freeNodeList ;
		pFifo->freeNodeList = node->next ;
	}
	
	memset(node,0x00,sizeof(MsgNode_t)) ;
	return node ;
}

int msgFifoPush(MsgFifo_t *pFifo,void *data)
{
	MsgNode_t *node = NULL;

	if(!pFifo || !data)
		return 0 ;
	
	if(pFifo->nodesNum == pFifo->maxNum)
		return 0 ;
	
	node = msgFifoNodeAlloc(pFifo) ;
	
	if(NULL==node)
		return 0 ;
	
	node->next = NULL;
	node->data = data ;

	if(pFifo->head == NULL && pFifo->tail == NULL)
	{
		pFifo->head = pFifo->tail = node ;
	}
	else
	{
		pFifo->tail->next = node ;
		pFifo->tail = node ;
	}
	
	pFifo->nodesNum ++ ;
	
	gCount++;
	printf("push %d:%s\n",gCount,(char*)data) ;

	return 1 ;
}

static void msgFifoNodeFree(MsgFifo_t *pFifo,MsgNode_t *pNode)
{
	if(!pFifo && !pNode)
		return;
	
	pNode->data = NULL;
	pNode->next = pFifo->freeNodeList ;
	pFifo->freeNodeList = pNode ;

	return ;
}

void *msgFifoPop(MsgFifo_t *pFifo)
{
	void * data = NULL ;
	MsgNode_t *pNode = NULL;

	if(!pFifo|| !pFifo->head)
		return NULL;
	
	pNode = pFifo->head ;
	pFifo->head = pNode->next ;

	data = pNode->data ;
	msgFifoNodeFree(pFifo,pNode) ;
	
	pFifo->nodesNum -- ;

	if(!pFifo->head) // notice
	{
		pFifo->tail= NULL;
		pFifo->nodesNum = 0 ;
	}
	printf("pop:%d:%s\n",gCount,(char*)data);
	gCount--;
	return data ;
}

void *msgFifoPeek(MsgFifo_t *pFifo)
{

	if(!pFifo || !pFifo->head)
	{
		return NULL ;
	}
	
	return pFifo->head->data ;
}

int msgFifoGetCurNum(MsgFifo_t *pFifo)
{
	if(!pFifo || !pFifo->head)
		return 0; 
	return pFifo->nodesNum ;
}

static void freeNodes(MsgFifo_t *pFifo)
{
	MsgNode_t *pNode = NULL ;
	
	if(!pFifo || pFifo->head)
		return ;
	
	while(pFifo->head)
	{
		pNode = pFifo->head ;
		pFifo->head = pNode->next ;
		if(pFifo->freeNodeFunc && pNode->data)
		{
			pFifo->freeNodeFunc(pNode->data,__FILE__,__LINE__) ;
		}
	}

	return ;
}

void msgFifoFree(MsgFifo_t *pFifo)
{
	if(!pFifo)
		return ;
	freeNodes(pFifo) ;

	if(pFifo->pmalloc)
		free(pFifo->pmalloc) ;
	
	free(pFifo);
}
