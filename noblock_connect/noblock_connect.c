#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define BUFFER_SIZE 1024 ;

int setNonBlocking(int fd)
{
	int oldOption = fcntl(fd,F_GETFL);
	int newOption = oldOption | O_NONBLOCK ;
	fcntl(fd,F_SETFL,newOption);
	return oldOption ;
}

int unblock_connect(const char *ip,int port,int time)
{
	int ret = 0 ;
	struct sockaddr_in servAddress;
	memset(&servAddress,0x00,sizeof(servAddress));
	servAddress.sin_family = AF_INET ;
	inet_pton(AF_INET,ip,&servAddress.sin_addr);
	servAddress.sin_port = htons(port);

	int sockfd = socket(PF_INET,SOCK_STREAM,0);
	assert(sockfd >=0);

	int fdopt = setNonBlocking(sockfd) ;

	ret = connect(sockfd,(struct sockaddr*)&servAddress,sizeof(servAddress));
	if(ret == 0)
	{
		printf("connect with server immediately\n");
		fcntl(sockfd,F_SETFL,fdopt);
		return sockfd ;
	}
	else if( errno != EINPROGRESS)
	{
		printf("unblock connect not support\n");
		close(sockfd);
		return -1;
	}


	fd_set writefds ;
	FD_ZERO(&writefds);

	FD_SET(sockfd,&writefds);

	struct timeval tv ;
	tv.tv_sec = time;
	tv.tv_usec = 0 ;

	ret = select(sockfd+1,NULL,&writefds,NULL,&tv);
	if(ret <= 0)
	{
		printf("connect time out or error %d\n",errno);
		close(sockfd);
		return -1;
	}

	if( !FD_ISSET(sockfd,&writefds))
	{
		printf("no events on sockfd found\n");
		close(sockfd);
		return -1 ;
	}

	int error = 0 ;
	int errLen = sizeof(error);

	ret = getsockopt(sockfd,SOL_SOCKET,SO_ERROR,&error,&errLen);
	if(ret < 0)
	{
		printf("get socket option failed\n");
		close(sockfd);
		return -1 ;
	}
	
	if(error != 0)
	{
		printf("connect failed after select with the error:%d\n",error);
		close(sockfd);
		return -1;
	}
	
	printf("connect success after select with the socket:%d\n",sockfd);
	fcntl(sockfd,F_SETFL,fdopt);
	return sockfd ;
}


int main(int argc ,char *argv[])
{
	if(argc <=2)
	{
		printf("usage :%s ip address port number\n",basename(argv[0]));
		return 1;
	}

	const char *ip = argv[1] ;
	int port = atoi(argv[2]);

	int sockfd = unblock_connect(ip,port,10);
	if(sockfd <0)
	{
		return 1 ;
	}
	close(sockfd);
	return 0 ;
}



