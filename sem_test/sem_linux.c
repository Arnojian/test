#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<sys/sem.h>

typedef union semun
{
	int val ;
	struct semid_ds *buf ;
	unsigned short *array ;
} SEMUN;

static int sem_id = 0 ;
static int set_semvalue() ;
static void del_semvalue() ;
static int semaphore_p() ;
static int semaphore_v() ;

int main(int argc , char * argv[])
{
	char message = 'X' ;
	int i = 0 ;
	
	sem_id = semget((key_t)1234,1,IPC_CREAT) ;
	if(argc > 1)
	{
		//程序第一次被调用，初始化信号量
		if( ! set_semvalue())
			{
				fprintf(stderr,"Failed to initialize semaphore \n") ;
				exit(EXIT_FAILURE) ;
			}
			//设置要输出到屏幕中的信息，即其参数的第一个字符
			message = argv[1][0] ;
			sleep(2) ;
	}
	
	for(i = 0 ;i< 10 ;++i)
	{
		if(! semaphore_p())
			{
				exit(EXIT_FAILURE) ;
			}
			
		printf("%c",message) ;
		fflush(stdout) ;
		sleep(rand()%3) ;
		
		printf("%c",message) ;
		fflush(stdout) ;
		
		if(!semaphore_v())
			{
				exit(EXIT_FAILURE) ;
			}
		sleep(rand()%2) ;
	}
	
	sleep(10) ;
	printf("\n%d-finished\n",getpid()) ;
	
	if(argc > 1)	
	{
		sleep(3) ;
		del_semvalue() ;
	}
	
	exit(EXIT_SUCCESS) ;
	
	
}
static int set_semvalue()
{
	SEMUN sem_union;
	sem_union.val = 1 ;
	if(semctl(sem_id,0,SETVAL,sem_union)==-1)
		return 0 ;
	return 1 ;
}
static void del_semvalue()
{
	SEMUN sem_union ;
	if(semctl(sem_id,0,IPC_RMID,sem_union)== -1)
		fprintf(stderr,"Failure to delete semaphore\n") ;
	
}
