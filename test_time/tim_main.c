#include <time.h>
#include <stdio.h>

#define MAXLINE 1024

int main(void)
{
	time_t calTime ;
	struct tm *tm ;

	char buf[MAXLINE] ;

	if( (calTime=time(NULL))==-1)
	{
		printf("time is error\n") ;
		return -1;
	}

	if( (tm = localtime(&calTime))==NULL)
	{
		printf("localtime is error\n") ;
		return -1 ;
	}

	if(strftime(buf,MAXLINE,"%a %b %d %X %Z %Y\n",tm)==0)
	{
		printf("strftime is error\n");
		return -1;
	}
	printf("time is :%s\n",buf) ;
	getchar() ;
	return 0 ;
}
