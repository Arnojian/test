#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

static void sig_user1(int signo)
{
	printf("sigusr1 function\n");
}

static void sig_user2(int signo)
{
	printf("sigusr2 function\n");
}

static void sig_int(int signo)
{
	printf("SIGINT function\n");
}

void sig_catch(int signo,void (*f)(int))
{
	struct sigaction sa ;
	sa.sa_handler = f ;
	sa.sa_flags = 0 ;
	sigemptyset(&sa.sa_mask);
	sigaction(signo,&sa,(struct sigaction*)0);
}

int main()
{
	sigset_t newmask,oldmask ;
	sig_catch(SIGUSR1,sig_user1);
	sig_catch(SIGUSR2,sig_user2);
	sig_catch(SIGINT,sig_int);

	sigemptyset(&newmask) ;
	sigaddset(&newmask,SIGUSR1);
	sigaddset(&newmask,SIGUSR2);
	sigaddset(&newmask,SIGINT);

	sigprocmask(SIG_BLOCK,&newmask,&oldmask);

	printf("SIGUSR is block\n\n");
	printf("send SIGUSR1,SIGUSR2,SIGINT\n\n");
	kill(getpid(),SIGUSR1);
	kill(getpid(),SIGUSR2);
	kill(getpid(),SIGUSR2);
	kill(getpid(),SIGINT);
	printf("SIGUSR is unblock\n\n");

	sigprocmask(SIG_UNBLOCK,&newmask,NULL);

	sigprocmask(SIG_SETMASK,&oldmask,NULL);

	return 0 ;
}


