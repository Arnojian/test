#include <sys/types.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void sig_usr(int signo);

int main(void)
{
	if( signal(SIGUSR1,sig_usr)==SIG_ERR)
	{
		printf("can't catch SIGUSR1\n") ;
		return -1 ;
	}
	if( signal(SIGUSR2,sig_usr)==SIG_ERR)
	{
		printf("can't cathc SIGUSR2\n") ;
		exit(1) ;
	}

	for(;;)
		pause();
	return 0 ;
}

static void sig_usr(int signo)
{
	if(signo == SIGUSR1)
	{
		printf("receive SIGUSR1\n") ;
	}
	if(signo == SIGUSR2)
	{
		printf("receive SIGUSR2\n");
	}
	return ;
}

