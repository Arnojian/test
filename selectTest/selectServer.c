#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<string.h>

int createSock(int port,char *ip)
{
	int sock ;
	int buffsize = 1024*64 ;
	int optive = 1 ;
	struct sockaddr_in servAddr ;
	sock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP) ;
	if(sock<0)
	{
		printf("[%s][%d]error create socket",__FILE__,__LINE__) ;
		return -1 ;
	}

	int ret = setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,(void*)&optive,sizeof(optive)) ;
	if(ret <0)
	{
		printf("[%s][%d]setsocket opt SO_REUSERADDR is error\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	ret = setsockopt(sock,SOL_SOCKET,SO_RCVBUF,&buffsize,sizeof(buffsize)) ;
	if(ret<0)
	{
		printf("[%s][%d]setsockopt is error SO_RECVBUF\n",__FILE__,__LINE__) ;
		return -1 ;
	}
	ret = setsockopt(sock,SOL_SOCKET,SO_SNDBUF,&buffsize,sizeof(buffsize)) ;
	if(ret<0)
	{
		printf("[%s][%d] setsockopt is error SO_SNDBUF\n",__FILE__,__LINE__);
		return -1 ;
	}

	bzero(&servAddr,sizeof(servAddr)) ;
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(port) ;		//�����ֽ���
	servAddr.sin_addr.s_addr= inet_addr(ip) ;//
	ret = bind(sock,(struct sockaddr *)&servAddr,sizeof(servAddr)) ;
	if(ret<0)
	{
		printf("[%s][%d]bind is error",__FILE__,__LINE__) ;
		close(sock) ;
		return -1 ;
	}
	
	return sock ;
}
int main()
{
	int port = 9999;
	char *ip = "127.0.0.1";
	int listenFd = -1 ;
	listenFd = createSock(port,ip) ;
	listen(listenFd,5) ;
	close(listenFd);
	return 0 ;
}











