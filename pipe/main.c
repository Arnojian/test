#include"main.h"
void client(int a,int b)
{
	size_t len ;
	size_t n ;
	char buff[50] ;
	printf("Please input the buffer you want\n") ;
	gets(buff) ;
	len = strlen(buff) ;
	if(buff[len-1] == '\n')
		len-- ;
	printf("the client write the data to server\n") ;
	
	write(b,buff,len) ;
	printf("the client read the data from server\n") ;
	n =read(a,buff,50) ;
	if(n > 0)
		printf("The client receive data from server: %s\n",buff) ;
	else
		printf("The client receive no data from the server\n") ;
	return ;
}
void server(int a,int b)
{
	size_t n ;
	char buff[50+1] ;
	if((n=read(a,buff,50))==0)
	{
		printf("end-of-line while reading pathname") ;
	}
	printf("The server read from client:%s\n",buff) ;
	buff[n] = '0' ;
	printf("The server send data: \"%s  \" to client \n",buff) ;
	n = write(b,buff,n) ;	
	if(n>0)
		printf("The data is sended!\n") ;
	else
		printf("send is error ,server,\n") ;
	return ;
}

int main(int agrc,char **argv)
{
	int pip1[2],pip2[2] ;
	pid_t childpid ;

	pipe(pip1);
	pipe(pip2);

	if( (childpid = fork())==0)//child //父进程返回子进程标志，子进程返回0!!!!
	{
		close(pip1[1]) ; //关闭管道1的写
		close(pip2[0]) ; //关闭管道2的读
		server(pip1[0],pip2[1]) ;
		exit(0) ;
	}
	close(pip1[0]) ; //parent关闭管道1的读
	close(pip2[1]) ; //parent关闭管道2的写
	client(pip2[0],pip1[1]) ; //执行client函数
	waitpid(childpid,NULL,0) ;
	exit(0) ;
	
}
