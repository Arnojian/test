#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>

static void sig_hup(int);
static void pr_ids(char *);

int main(void)
{
	pid_t pid ;
	char c ;
	pr_ids("parent") ;

	if( (pid=fork())<0)
	{
		printf("err fork\n") ;
		return -1 ;
	}
	else if(pid>0)
	{
		sleep(5) ;
		exit(0) ;
	}
	else
	{
		pr_ids("child") ;
		signal(SIGHUP,sig_hup) ;
		kill(getpid(),SIGTSTP) ;
		pr_ids("child") ;
		if( read(0,&c,1)!= 1)
		{
			printf("read error from control\n");
		}
		exit(0);
	}

	return 0 ;
}

static void sig_hup(int signo)
{
	printf("SIGHUP received,pid=%d\n",getpid());
	return ;
}
static void pr_ids(char *name)
{
	printf("%s:pid=%d,ppid=%d,pgrp=%d\n",\
			name,getpid(),getppid(),getpgrp());
	return ;
}


