#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

/*
 * vfork 子进程与父进程共享同一块内存空间；
 * 并不像fork一样将父进程的地址空间完全复制到子进程中，因为子进程会调用
 * exec(或exit),于是也就不会存访该地址空间。
 * 不过子进程在调用exec(或exit)之前将在父进程地址空间中运行；
 * 如果子进程调用了exit 会关闭所有标准I/O流；
 * 而_exit函数并不执行标准I/O缓存的刷新操作；
 *
 * */

int glob = 6 ;
int main()
{
	int var ;
	pid_t	pid ;
	var = 88 ;
	printf("before vfork\n") ;
	if((pid=vfork())<0)
	{
		printf("vfork error\n");
		return -1 ;
	}
	else if(pid==0)
	{
		glob++ ;
		var ++ ;
		_exit(0) ; //如果使用的是exit，那么输出结果将不会有 printf 
	}
	/*parent*/
	printf("pid= %d,glob=%d,var=%d\n",pid,glob,var) ;
	exit(0) ;
}


