#include<unistd.h>
#include<dirent.h>
#include<string.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<stdio.h>
#include<fcntl.h>

/*
			struct dirent
			{
			   long d_ino;  inode number 索引节点号
			   off_t d_off;  offset to this dirent 在目录文件中的偏移 
			   unsigned short d_reclen;  length of this d_name 文件名长 
			   unsigned char d_type;  the type of d_name 文件类型 
			   char d_name [NAME_MAX+1];  file name (null-terminated) 文件名，最长255字符 
			}
			
			2.
			struct __dirstream
			{
				void *__fd;  `struct hurd_fd' pointer for descriptor.   
				char *__data;  Directory block.   
				int __entry_data;  Entry number `__data' corresponds to.   
				char *__ptr;  Current pointer into the block.   
				int __entry_ptr;  Entry number `__ptr' corresponds to.   
				size_t __allocation;  Space allocated for the block.   
				size_t __size;  Total valid data in the block.   
				__libc_lock_define (, __lock)  Mutex lock for this structure.   
			};
			
			typedef struct __dirstream DIR;

*/
void printfdir(char *dir,int depth)
{
	DIR *dp ;
	struct dirent *entry ;
	struct stat statbuf ;
	
	if((dp = opendir(dir))==NULL)
	{
		fprintf(stderr,"Cannot open directory: %s\n",dir) ;
	}
	
	chdir(dir) ;//切换此目录为当前工作目录
	
	while((entry=readdir(dp))!=NULL)
	{
			lstat(entry->d_name,&statbuf) ;
		
			
			if(S_ISDIR(statbuf.st_mode))
			{
				if(strcmp(".",entry->d_name)==0||strcmp("..",entry->d_name)==0)
					continue ;
				printf("%*s%s/\n",depth,"",entry->d_name) ;
				printfdir(entry->d_name,depth+4) ;
			}
			else
				printf("%*s%s/\n",depth,"",entry->d_name) ;
	}
		
	chdir("..") ;
	closedir(dp) ;
}
	
int main()
{
		printf("Directory scan of /home/arno:\n") ;
		printfdir("/home/arno/test/unistd",0) ;
		printf("done.\n") ;
		exit(0) ;
}
