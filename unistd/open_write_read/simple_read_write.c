#include<unistd.h>    //open read write close
#include<stdio.h>
#include<sys/stat.h>  //文件信息
#include<fcntl.h>     //
#include<stdlib.h>    //系统函数exit等
#include<errno.h>     //错误 errno等信息

int main()
{

	int in,out ;
	
	char c ;

	in = open("file.in",O_RDONLY) ;  //文件的复制
	out = open("file.out",O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR) ;
	if(out<0)
    {
	     printf("%d\n",errno) ;
	     exit(-1) ;
	}
	printf("file.out fd is : %d\n",out) ;
	while(read(in,&c,1)==1)
		write(out,&c,1) ;
	exit(0) ;
}
