#include<unistd.h>
#include<stdio.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<fcntl.h>
#include<stdlib.h>
#include<errno.h>


int main()
{
	char block[1024] ;
	int in ,out;
	ssize_t nread ;
	
	in = open("file.in",O_RDONLY) ;
	if(in < 0)
	{
		printf("open file.in is error : %d\n",errno) ;
		exit(-1);
	}
	out = open("file.out",O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR) ;
	if(out <0)
	{
		printf("create file.out is error: %d\n",errno) ;
		exit(-1) ;
	}
	
	while( (nread = read(in,block,sizeof(block)))>0)
		write(out,block,nread) ;
		
	exit(0) ;
}
