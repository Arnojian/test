#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
int main()
{
		int c ;
		FILE *in ,*out ;
		
		in = fopen("file.in","r");
		if(NULL ==in)
		{
			printf("fopen file.in is error : %d\n",errno) ;
			exit(-1) ;
		}
		out = fopen("file.out","w") ;
		if(NULL == out)
		{
			printf("fopen file.out is error : %d\n",errno) ;
			exit(-1) ;
		}
		
		while((c=fgetc(in))!=EOF)
			fputc(c,out) ;
		if(fclose(out) == EOF)
		{
				printf("fclose file.out is error :%d\n",errno) ;
				exit(-1);
		}
		if(EOF==fclose(in))
		{
			printf("fclose file.in is error: %d\n",errno) ;
			exit(-1) ;
		}
		
		exit(0);
}
