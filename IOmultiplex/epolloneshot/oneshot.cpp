#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <pthread.h>

#define MAX_EVENTS_NUMBER 1024
#define BUFFER_SIZE 1024

struct fds
{
	int epollfd;
	int sockfd ;
};

int setNonBlocking(int fd)
{
	int oldOption = fcntl(fd,F_GETFL);
	int newOption = oldOption | O_NONBLOCK;
	fcntl(fd,F_SETFL,newOption);
	return oldOption;
}

void addFd(int epollfd,int sockfd,bool oneshot)
{
	epoll_event event ;
	event.data.fd = sockfd;
	event.events = EPOLLIN | EPOLLET ;
	if(oneshot)
		event.events |= EPOLLONESHOT;
	epoll_ctl(epollfd,EPOLL_CTL_ADD,sockfd,&event);
	setNonBlocking(sockfd);
}

void resetOneShot(int epollfd,int fd)
{
	epoll_event event ;
	event.data.fd = fd ;
	event.events = EPOLLIN| EPOLLET | EPOLLONESHOT;
	epoll_ctl(epollfd,EPOLL_CTL_MOD,fd,&event);
}

void * worker(void *arg)
{
	int sockfd = ((fds*)arg)->sockfd ;
	int epollfd = ((fds*)arg)->epollfd;

	printf("start new thread to receive data on fd:%d\n",sockfd);
	char *buf[BUFFER_SIZE] ;
	memset(buf,0x00,BUFFER_SIZE);
	// 循环读取数据，直到遇到EAGAIN错误
	while(1)
	{
		int ret = recv(sockfd,buf,BUFFER_SIZE-1,0);
		if(ret ==0)
		{
			epoll_ctl(epollfd,EPOLL_CTL_DEL,sockfd,NULL);
			close(sockfd);
			printf("foreiner closed the connection\n");
			break ;
		}
		else if(ret <0)
		{
			if(errno == EAGAIN)
			{
				resetOneShot(epollfd,sockfd);
				printf("read later\n");
				break ;
			}
		}
		else
		{
			printf("get content:%s\n",buf);
			sleep(5);
		}
	}
	printf("end thread receiving data on fd:%d\n",sockfd);
	//pthread_exit(0);
}

int main(int argc,char* argv[])
{
	if(argc <=2)
	{
		printf("usage:%s ip_address,port_number\n",basename(argv[0]));
		return 1;
	}

	const char *ip = argv[1];
	int port = atoi(argv[2]);

	int ret = 0 ;
	struct sockaddr_in address;
	bzero(&address,sizeof(address));

	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	inet_pton(AF_INET,ip,&address.sin_addr);

	int listenfd = socket(PF_INET,SOCK_STREAM,0);
	assert(listenfd>=0);

	ret = bind(listenfd,(struct sockaddr*)&address,sizeof(address));
	assert(ret >=0);

	ret = listen(listenfd,5);
	assert(ret >=0);

	epoll_event events[MAX_EVENTS_NUMBER];

	int epollfd  = epoll_create(1024);
	assert(epollfd != -1);

	addFd(epollfd,listenfd,true);

	while(1)
	{
		int ret = epoll_wait(epollfd,events,MAX_EVENTS_NUMBER,-1);
		if(ret <0)
		{
			printf("epoll failure errno:%s\n",strerror(errno));
			break ;
		}
		for(int i = 0 ;i<ret ;i++)
		{
			int sockfd = events[i].data.fd ;
			if(sockfd == listenfd)
			{
				struct sockaddr_in cliAddress;
				memset(&cliAddress,0x00,sizeof(cliAddress));
				socklen_t cliLen = sizeof(cliAddress);

				int connfd = accept(listenfd,(struct sockaddr*)&cliAddress,&cliLen);
				assert(connfd >=0);
				addFd(epollfd,connfd,true);
			}
			else if(events[i].events & EPOLLIN)
			{
				pthread_t thread;
				fds fdsWorker ;
				fdsWorker.epollfd = epollfd ;
				fdsWorker.sockfd = sockfd ;

				pthread_create(&thread,NULL,worker,(void *)&fdsWorker);
				//pthread_detach(thread);
			}
			else
			{
				printf("something else happened\n");
			}
		}
	}
}


