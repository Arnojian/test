#include<stdio.h>
#include<stdlib.h>
#include<errno.h>

#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/epoll.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>

#define IP "127.0.0.1"
#define PORT 9090
#define MAXLINE 1024



int handleConnection(int sockfd)
{
	
	char buf[MAXLINE] ="hello Mr.Luo!" ;
	int ret = write(sockfd,buf,strlen(buf)) ;
	if (ret < 0)
	{
		printf("error:%d\n",errno) ;
		close(sockfd) ;
		return -1 ;
	}

	ret = read(sockfd,buf,MAXLINE) ;
	buf[ret] = '\0' ;
	printf("recv message :%s\n",buf) ;
	return 0 ;

}


int main()
{
	int sockfd ;
	struct sockaddr_in servAddr ;
	
	sockfd = socket(AF_INET,SOCK_STREAM,0) ;

	bzero(&servAddr,sizeof(servAddr) ) ;
	servAddr.sin_family= AF_INET ;
	inet_pton(AF_INET,IP,&servAddr.sin_addr) ;
	servAddr.sin_port = htons(PORT) ;

	int ret = connect(sockfd,(struct sockaddr*)&servAddr,sizeof(servAddr)) ;
	if (ret < 0)
	{
		printf("close the socket,when connect is failed\n") ;
		close(sockfd) ;
		exit(-1) ;
	}

	handleConnection(sockfd) ;
	close(sockfd) ;
	
	return 0 ;
}


