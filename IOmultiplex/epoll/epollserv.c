#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>
#include<stdlib.h>

#include<arpa/inet.h>
#include<netinet/in.h>
#include<sys/epoll.h>
#include<sys/types.h>
#include<sys/socket.h>

#define IP "127.0.0.1"
#define PORT 9090
#define MAXLINE 1024
#define MAXLISTEN 5

/*
	@funcationname: createAndBind
	@description:create socket and bind the addresss
	@return
		succ: the valid of socket
		fail: -1
*/
int createAndBind()
{	
	int listenfd ;
	struct sockaddr_in servAddr ;

	bzero(&servAddr,sizeof(servAddr)) ;

	listenfd = socket(AF_INET,SOCK_STREAM,0) ;
	if (listenfd < 0)
	{
		printf("%s %d error create the listen socket,err:%d\n",__FILE__,__LINE__,errno) ;
		return -1 ;
	}

	servAddr.sin_family = AF_INET ;
	inet_pton(AF_INET,IP,&servAddr.sin_addr) ;
	servAddr.sin_port = htons(PORT);
	int ret = bind(listenfd,( struct sockaddr *)&servAddr,sizeof(servAddr)) ;
	if (ret < 0)
	{
		printf("%s %d, error bind the address,err:%d\n",__FILE__,__LINE__,errno) ;
		return -1 ;
	}

	ret = listen(listenfd,MAXLISTEN) ;
	if (ret < 0)
	{
		printf("%s %d, error listen \n",__FILE__,__LINE__) ;
		return -1 ;
	}
	
	int bReuseAddr = 1 ;
	ret = setsockopt(listenfd,SOL_SOCKET,SO_REUSEADDR,(const char *)&bReuseAddr,sizeof(bReuseAddr)) ;
	if (ret < 0)
	{
		printf("%s %d,error setsockopt SO_REUSEADDR\n",__FILE__,__LINE__) ;
		return -1 ;
	}

	return listenfd ;
}

/*
	@functionname: epollAddEvent
	@decription:   add the fd's event
	@input	: 
		epollfd : epollfd
		clientfd: client socket
		state :   the event need to add to clientfd
	@return 
		succ : 0
		fail : -1
*/
int epollAddEvent(int epollfd,int clientfd,int state)
{
	struct epoll_event event ;
	event.data.fd = clientfd ;
	event.events = state ;

	return epoll_ctl(epollfd,EPOLL_CTL_ADD,clientfd,&event) ;
}

/*
	@functionname: epollModEvent
	@decription:   Modify the fd's event
	@input	: 
		epollfd : epollfd
		clientfd: client socket
		state :   the event need to modify clientfd
	@return 
		succ : 0
		fail : -1
*/
int epollModEvent(int epollfd,int clientfd,int state)
{
	struct epoll_event event ;
	event.data.fd = clientfd ;
	event.events = state ;

	return epoll_ctl(epollfd,EPOLL_CTL_MOD,clientfd,&event) ;
}

/*
	@functionname: epollDelEvent
	@decription:   Delete the fd's event
	@input	: 
		epollfd : epollfd
		clientfd: client socket
		state :   the event need to delete from clientfd
	@return 
		succ : 0
		fail : -1
*/
int epollDelEvent(int epollfd,int clientfd,int state)
{
	struct epoll_event event ;
	event.data.fd = clientfd ;
	event.events = state ;
	
	return epoll_ctl(epollfd,EPOLL_CTL_DEL,clientfd,&event) ;
}

/*
	@functionname: handlAccept
	@decription:   handle the listen socket
	@input	: 
		epollfd : epollfd
		listenfd: the listen socket 
	@return 
		succ : 0
		fail : -1
*/
int handleAccept(int epollfd,int listenfd)
{
	int connfd = -1 ;
	struct sockaddr_in cliAddr ;
	socklen_t cliAddrLen ;
	int ret = -1 ;

	//cliAddrLen = sizeof(cliAddr) ;

	memset(&cliAddr,0,sizeof(cliAddr)) ;
	
	connfd = accept(listenfd,(struct sockaddr*)&cliAddr,&cliAddrLen) ;
	if (connfd < 0)
	{
		printf("%s %d,error accept .err:%d\n",__FILE__,__LINE__,errno) ;
		return -1 ;
	}
	else
	{
		printf("%s,%d accept connfd is ok\n",__FILE__,__LINE__) ;
		ret = epollAddEvent(epollfd,connfd,EPOLLIN) ;
		if (ret < 0)
		{
			printf("%s %d, epollAddEvent is error\n",__FILE__,__LINE__) ;
			return -1 ;
		}
		return 0 ;
	}
}

char buf[MAXLINE] ;

int doRead(int epollfd,int cliConn)
{
	int nread  ;
	memset(buf,0,MAXLINE) ;
	nread = read(cliConn,buf,MAXLINE) ;
	if (nread < 0) //something is error
	{
		if (EINTR == errno)
		{
			printf("%s %d read is interrupt\n",__FILE__,__LINE__) ;
			return -1 ;
		}
		else
		{
			printf("%s %d,error read,err:%d\n",__FILE__,__LINE__,errno) ;
			close(cliConn) ;
			epollDelEvent(epollfd,cliConn,EPOLLIN) ;
			return -1 ;
		}
	}
	else if (nread==0) // the connection is close
	{
		printf("%s %d,error read,err:%d\n",__FILE__,__LINE__,errno) ;
		close(cliConn) ;
		epollDelEvent(epollfd,cliConn,EPOLLIN) ;
		return 0 ;
	}
	else // read the data
	{
		buf[nread] = '\0' ;
		printf("recv message:%s\n",buf) ;
		epollModEvent(epollfd,cliConn,EPOLLOUT);
		return 0 ;
	}
}

int doWrite(int epollfd,int cliConn)
{
	int nWrite ;

	nWrite = write(cliConn,buf,strlen(buf)) ; // you can use writen
	if (nWrite < 0) //error
	{	
		if(errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) 
		{
			return -1 ;
		}	
		else
		{
			printf("%s %d write is error,%d\n",__FILE__,__LINE__,errno) ;
			close(cliConn) ;
			epollDelEvent(epollfd,cliConn,EPOLLOUT) ;
			return -1;
		}
	}
	else //write success
	{
		printf("%s,%d,write is ok\n",__FILE__,__LINE__) ;
		epollModEvent(epollfd,cliConn,EPOLLIN) ;
		return 0 ;
	}
}

/*
	@functionname: doEPoll
	@decription:   the main loop of epoll
	@input	: 
		epollfd : epollfd
		listenfd: listen socket
	@return 
		succ : 0
		fail : -1
*/
int doEpoll(int epollfd,int listenfd)
{
	struct epoll_event events[MAXLINE] ;
	int ret = -1 ;
	int i = 0 ;

	for(;;)
	{
		ret = epoll_wait(epollfd,events,MAXLINE,-1) ; // it is not weekup util someone's event is active or the timeout is reached!
		if (ret < 0)
		{
			if (EINTR == errno)
			{
				printf("%s %d, epoll is interrupt:%d\n",__FILE__,__LINE__,errno) ;
				continue ;
			}
			else
			{
				close(epollfd) ;
				return -1 ;
			}
		}
		else if (ret == 0) //timeout
		{
			printf("%s %d,epoll is timeout\n",__FILE__,__LINE__) ;
			continue ;
		}
		else //ok
		{
			for (i = 0 ;i<ret ; i++)
			{
				if( (events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP))
				{
					printf("%s %d, error socket\n",__FILE__,__LINE__) ;
					close(events[i].data.fd) ;
					continue ;
				}
				else if( (events[i].data.fd == listenfd) && (events[i].events & EPOLLIN))
				{
					handleAccept(epollfd,listenfd) ;
				}
				else if( events[i].events & EPOLLIN)
				{
					doRead(epollfd,events[i].data.fd) ;
				}
				else if (events[i].events & EPOLLOUT)
				{
					doWrite(epollfd,events[i].data.fd) ;
				}
			} // end for ret
		} // end if ret
	} //end for(;;)
}

int main()
{
	int listenfd ;
	int epollfd ;

	epollfd = epoll_create(1000) ;
	if (epollfd < 0)
	{
		printf("%s,%d error create epollfd:%d\n",__FILE__,__LINE__,errno) ;
		exit(-1) ;
	}

	listenfd = createAndBind() ;
	if (listenfd < 0)
	{
		exit(-1) ;
	}

	int ret = epollAddEvent(epollfd,listenfd,EPOLLIN) ;
	if (ret < 0)
	{
		printf("%s %d, error add epoll event listenfd,err:%d\n",__FILE__,__LINE__,errno) ;
		exit(-1) ;
	}

	doEpoll(epollfd,listenfd) ;

	return 0 ;

}