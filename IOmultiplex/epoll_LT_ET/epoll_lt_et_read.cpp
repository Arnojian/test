#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/epoll.h>
#include <fcntl.h>

#define MAX_EVENTS_NUMBER 1024 
#define BUFFER_SIZE 1024

int setNonBlocking(int fd)
{
	int oldOption = fcntl(fd,F_GETFL);
	int newOption = oldOption | O_NONBLOCK;
	fcntl(fd,F_SETFL,&newOption);
	return oldOption;
}
// 默认是EPOLLLT模式
void addFd(int epollfd,int fd,bool enable)
{
	epoll_event event ;
	event.data.fd = fd ;
	event.events = EPOLLIN ;
	if(enable)
	{
		event.events |= EPOLLET ;
	}

	epoll_ctl(epollfd,EPOLL_CTL_ADD,fd,&event);
	setNonBlocking(fd);
}

void lt(epoll_event *events,int number,int epollfd,int listenfd)
{
	char buf[BUFFER_SIZE];
	for(int i = 0 ;i<number;i++)
	{
		int sockfd = events[i].data.fd ;
		if(sockfd == listenfd)
		{
			struct sockaddr_in cliAddress ;
			memset(&cliAddress,0x00,sizeof(cliAddress));
			socklen_t cliLen = sizeof(cliAddress);
			int connfd = accept(sockfd,(struct sockaddr*)&cliAddress,&cliLen) ;
			addFd(epollfd,connfd,false);
		}
		else if(events[i].events & EPOLLIN)
		{
			printf("event trigger once\n");
			memset(buf,0x00,BUFFER_SIZE);
			int ret = recv(sockfd,buf,BUFFER_SIZE,0);
			if(ret <=0)
			{
				epoll_ctl(epollfd,EPOLL_CTL_DEL,sockfd,NULL);
				close(sockfd);
				continue ;
			}
			printf("get %d bytes of content :%s\n",ret,buf);
		}
		else
		{
			printf("something else happend\n");
		}
	}
}

void et(epoll_event *events,int number,int epollfd,int listenfd)
{
	char buf[BUFFER_SIZE];
	for(int i = 0 ;i<number;i++)
	{
		int sockfd = events[i].data.fd ;
		if(sockfd==listenfd)
		{
			struct sockaddr_in cliAddress ;
			socklen_t cliLen = sizeof(cliAddress);
			memset(&cliAddress,0x00,cliLen);

			int connfd = accept(sockfd,(struct sockaddr*)&cliAddress,&cliLen);
			if(connfd <0)
			{
				continue ;
			}
			addFd(epollfd,sockfd,true);
		}
		else if(events[i].events & EPOLLIN)
		{
			// 这段代码不会被重复触发，所以需要一次性把数据全部读取出来
			printf("event trigger once\n");
			memset(buf,0x00,BUFFER_SIZE);
			int readbytes = 0 ;
			while(1)
			{
				int ret = recv(sockfd,buf+readbytes,BUFFER_SIZE-readbytes,0);
				if(ret <0)
				{
					if(errno == EAGAIN)
					{
						printf("read later\n");
						break ;
					}
					epoll_ctl(epollfd,EPOLL_CTL_DEL,sockfd,0);
					close(sockfd);
					break ;
				}
				else if(ret == 0 )
				{
					epoll_ctl(epollfd,EPOLL_CTL_DEL,sockfd,0);
					close(sockfd);
					break;
				}
				else
				{
					readbytes+=ret ;
				}
			}
			printf("get %d bytes of content :%s\n",readbytes,buf);
		}
		else
		{
			printf("something else happended\n");
		}
	}
}

int main(int argc,char* argv[])
{
	if(argc <3)
	{
		printf("usage %s ip_address,port_number, lt or et\n");
		return 1;
	}

	const char *ip = argv[1];
	int port = atoi(argv[2]);
	int etEnable = atoi(argv[3]);

	int ret = 0 ;
	struct sockaddr_in servAddress ;
	memset(&servAddress,0x00,sizeof(servAddress));
	servAddress.sin_family = AF_INET;
	servAddress.sin_port = htons(port);
	inet_pton(AF_INET,ip,&servAddress.sin_addr);

	int listenfd = socket(PF_INET,SOCK_STREAM,0);
	assert(listenfd >=0);

	ret = bind(listenfd,(struct sockaddr*)&servAddress,sizeof(servAddress));
	assert(ret !=-1);

	ret = listen(listenfd,5);
	assert(ret !=-1);

	epoll_event events[MAX_EVENTS_NUMBER];

	int epollfd = epoll_create(1000);
	assert(epollfd !=-1);
	if(etEnable==1)
		addFd(epollfd,listenfd,true);
	else
		addFd(epollfd,listenfd,false);

	while(1)
	{
		int ret = epoll_wait(epollfd,events,MAX_EVENTS_NUMBER,-1);
		if(ret <0)
		{
			printf("epoll failure\n");
			break ;
		}
		if(etEnable==1)
			et(events,ret,epollfd,listenfd);
		else
			lt(events,ret,epollfd,listenfd);
	}

	close(listenfd);
	return 0 ;
}

