#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<netinet/in.h>
#include<sys/socket.h>
#include<sys/types.h>

#define IP "127.0.0.1"
#define PORT 9090
#define MAXLINE 1024

int main()
{
	struct sockaddr_in servAddr ;
	socklen_t servAddrLen ;
	int servfd = -1 ;
	char buf[MAXLINE] = "luojian love liya" ;
AGAIN:
	bzero(&servAddr,sizeof(struct sockaddr_in)) ;
	servAddrLen = sizeof(struct sockaddr) ;

	servAddr.sin_family = AF_INET ;
	//inet_pton(AF_INET,IP,&servAddr.sin_addr);
	servAddr.sin_addr.s_addr = inet_addr(IP) ;
	servAddr.sin_port = htons(PORT) ;
	
	servfd = socket(AF_INET,SOCK_STREAM,0) ;
	
	int connfd = connect(servfd,(struct sockaddr*)&servAddr,servAddrLen) ;
	if (connfd < 0)
	{
		if (errno == EINTR)
		{
			close(connfd) ;
			goto  AGAIN;
		}
		else
		{
			printf("error connect to serv:%d\n",errno) ;
			exit(-1) ;
		}
	}
	sleep(1);
	write(servfd,buf,strlen(buf)+1) ;
	memset(buf,0,MAXLINE);

	read(servfd,buf,MAXLINE) ;
	printf("read data is :%s\n",buf) ;

	return 0 ;
}