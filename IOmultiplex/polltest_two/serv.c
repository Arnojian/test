#include<stdio.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>

#include<arpa/inet.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<poll.h>
#include<unistd.h>


#define IPADDRESS "127.0.0.1"
#define PORT	  9090
#define LISTENNUM 5
#define MAXOPEN   1000

/*
	@funcation name : socketBind 
	@description: create socket and bind the address
	@return
		succ : listenfd 
		fail : -1 
*/
int socketBind(char *ip,int port)
{
	int listenfd ;
	struct sockaddr_in servAddr ;

	listenfd  = socket(AF_INET,SOCK_STREAM,0) ;
	if (listenfd < 0)
	{
		printf("listenfd create is error:%d\n",errno) ;
		return -1 ;
	}

	bzero(&servAddr,sizeof(struct sockaddr_in)) ;
	
	servAddr.sin_family= AF_INET ;
	//servAddr.sin_addr.s_addr = inet_addr(ip) ;
	inet_pton(AF_INET,ip,&servAddr.sin_addr) ;
	servAddr.sin_port = htons(port) ;
	
	
	int ret = bind(listenfd,(struct sockaddr*)&servAddr,sizeof(servAddr)) ;
	if (ret < 0)
	{
		printf("bind is error:%d\n",errno) ;
		return -1 ;
	}

	return listenfd ;

}

int handleConnection(struct pollfd *connfds,int num)
{
	int i , n ;
	char buf[1024] ;
	memset(buf,0,1024);
	printf("deal the conn num:%d\n",num) ;
	for(i = 1 ;i<=num;i++)
	{
		if(connfds[i].fd < 0)
			continue ;

		if(connfds[i].revents & POLLIN)
		{
AGAIN:
			n = read(connfds[i].fd,buf,1024) ;
			if(n <0)
			{
				if (errno == EINTR)
				{
					goto AGAIN;
				}
				else
				{
					close(connfds[i].fd) ;
					printf("close a conn index:%d,err:%d\n",i,errno) ;
					connfds[i].fd = -1 ;
				}
			}
			else if(n==0)
			{
				printf("the socket is closed by the peer\n");
				close(connfds[i].fd) ;
				connfds[i].fd = -1 ;
			}
			else
			{
				buf[n] = '\0' ;
				printf("recv data is :%s\n",buf) ;
				write(connfds[i].fd,buf,n) ;
			}
		}
	}
	return 0 ;
}

int doPoll(int listenfd)
{
	int nReady = 0 ;
	int maxI = 0 ;
	int connfd =-1 ;
	int i = 0;
	struct sockaddr_in cliAddr ;
	socklen_t cliAddrLen ;

	struct pollfd clientfds[MAXOPEN] ;
		
	cliAddrLen = sizeof(struct sockaddr) ;
	
	memset(&cliAddr,0,sizeof(struct sockaddr_in)) ;

	clientfds[0].fd = listenfd ;
	clientfds[0].events = POLLIN  ;
	clientfds[0].revents = 0 ;
	
	for (i = 1 ;i<MAXOPEN ;i++ )
	{
		clientfds[i].fd = -1 ;
	}
	
	for(;;)
	{
		printf("poll start...\n") ;
		nReady = poll(clientfds,maxI+1,-1) ;
		if(nReady ==-1)
		{
			if(errno == EINTR)
				continue ;
			else
				exit(-1) ;
		}
		
		
		if(clientfds[0].revents & POLLIN)
		{
			printf("accept the connectin\n") ;
			
			connfd = accept(listenfd,(struct sockaddr*)&cliAddr,&cliAddrLen) ;
			printf("connfd:%d\n",connfd) ;
			if (connfd < 0)
			{
				printf("error:%d\n",errno) ;
				if (errno == EINTR)
				{
					continue ;
				}
				else
				{
					printf("some error:%d\n",errno) ;
					exit(-1) ;
				}
			}
			printf("accept a new client:%s,%d\n",inet_ntoa(cliAddr.sin_addr),ntohs(cliAddr.sin_port)) ;

			for(i= 1 ;i<MAXOPEN;i++)
			{
				if(clientfds[i].fd < 0)
				{
					clientfds[i].fd = connfd ;
					break ;
				}
			}

			if (i == MAXOPEN)
			{
				printf("too many clients.\n") ;
				exit(-1) ;
			}

			clientfds[i].events = POLLIN ;
			clientfds[i].revents = 0 ;

			maxI = (i > maxI) ? i:maxI;
			
			if(--nReady <= 0 )
				continue ;
		}
		else
		{
			printf("deal a conn\n") ;
			handleConnection(clientfds,maxI) ;
		}
		
	}

	return 0 ;
}

int main()
{
	int listenfd ;
	
	printf("init is ok\n") ;

	listenfd = socketBind(IPADDRESS,PORT) ;
	if (listenfd == -1)
	{
		return -1 ;
	}

	printf("create socket and bind is ok\n") ;
	int ret = listen(listenfd,LISTENNUM) ;
	if (ret < 0)
	{
		printf("listen is error\n") ;
		exit(-1) ;
	}
	printf("deal the poll\n") ;
	doPoll(listenfd) ;

	printf("error\n") ;
}

