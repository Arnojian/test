/*
 ============================================================================
 Name        : epollTest.c
 Author      : arno.luo
 Version     :
 Copyright   : Your copyright notice
 Description : epollTest in C
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include<errno.h>

#include<netinet/in.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<sys/epoll.h>
#include<unistd.h>
#include<sys/types.h>

#define IPADDRESS "127.0.0.1"
#define PORT		9090
#define MAXSIZE		1024
#define LISTENQ		5
#define FDSIZE		1000
#define EPOLLEVENTS	100
//创建套接字并绑定
int socket_bind(const char *ip,int port) ;
//io多路复用epoll
void do_epoll(int listenfd) ;
//事件处理函数
void handle_events(int epollfd,struct epoll_event *events,int num,int listenfd,char *buf) ;
//处理接收到的连接
void handle_accept(int epollfd,int listenfd) ;
//读处理
void do_read(int epollfd,int fd,char *buf) ;
//写处理
void do_write(int epollfd,int fd,char *buf) ;
//添加事件
void add_event(int epollfd,int fd,int  state) ;
//删除事件
void del_event(int epollfd,int fd,int state) ;
//更新事件
void modify_event(int epollfd,int fd,int state) ;
int main(void) {
	int listenfd =-1 ;
	listenfd = socket_bind("127.0.0.1",9090) ;
	listen(listenfd,LISTENQ) ;
	do_epoll(listenfd) ;
	return EXIT_SUCCESS;
}

int socket_bind(const char *ip,int port)
{
	int servfd = -1 ;
	struct sockaddr_in servaddr ;
	bzero(&servaddr,sizeof(servaddr)) ;

	servaddr.sin_family = AF_INET ;
	if(-1==(inet_pton(AF_INET,ip,&servaddr.sin_addr)))
	{
		printf("inet_pton is error:%d",errno) ;
		exit(1) ;
	}
	servaddr.sin_port = htons(port) ;

	servfd = socket(AF_INET,SOCK_STREAM,0) ;

	if(-1==(bind(servfd,(struct sockaddr*)&servaddr,sizeof(servaddr))))
	{
		printf("the bind is error : %d\n",errno) ;
		exit(1) ;
	}

	return servfd ;
}
void add_event(int epollfd,int fd,int state)
{
	struct epoll_event ev ;
	ev.events = state ;
	ev.data.fd = fd ;
	epoll_ctl(epollfd,EPOLL_CTL_ADD,fd,&ev) ;
}
void del_event(int epollfd,int fd,int state)
{
	struct epoll_event ev ;
	ev.events = state ;
	ev.data.fd = fd ;
	epoll_ctl(epollfd,EPOLL_CTL_DEL,fd,&ev) ;
}
void modify_event(int epollfd,int fd,int state)
{
	struct epoll_event ev ;
	ev.events = state ;
	ev.data.fd = fd ;
	epoll_ctl(epollfd,EPOLL_CTL_MOD,fd,&ev) ;
}
void do_epoll(int listenfd)
{
	int epollfd = -1 ;
	struct epoll_event events[EPOLLEVENTS] ;
	int ret ;
	char buf[MAXSIZE] ;
	memset(buf,0,MAXSIZE) ;

	epollfd = epoll_create(FDSIZE) ;
	add_event(epollfd,listenfd,EPOLLIN) ;

	for(;;)
	{
		ret = epoll_wait(epollfd,events,EPOLLEVENTS,-1) ;
		handle_events(epollfd,events,ret,listenfd,buf) ;
	}

}
void handle_events(int epollfd,struct epoll_event *events,int num,int listenfd,char *buf)
{
	int i = 0 ;
	int fd =-1 ;

	for( i = 0 ;i< num; i++)
	{
		fd = events[i].data.fd ;
		if((fd == listenfd)&&(events[i].events & EPOLLIN))
				handle_accept(epollfd,listenfd) ;
		else if(events[i].events & EPOLLIN)
			do_read(epollfd,fd,buf) ;
		else if(events[i].events&EPOLLOUT)
			do_write(epollfd,fd,buf) ;
	}
}
void do_write(int epollfd,int fd,char *buf)
{
	int nwrite = 0 ;
	nwrite = write(fd,buf,strlen(buf)) ;
	if(-1==nwrite)
	{
		printf("write is error :%d",errno) ;
		close(fd) ;
		del_event(epollfd,fd,EPOLLOUT) ;
	}
	else
	{
		modify_event(epollfd,fd,EPOLLIN) ;
	}
}
void do_read(int epollfd,int fd, char *buf)
{
	int nread = 0 ;
	nread = read(fd,buf,MAXSIZE) ;
	buf[nread] = '\0' ;
	nread++ ;
	if(-1== nread)
	{
		printf("socket :%d  read is error:%d",fd,errno) ;
		close(fd) ;
		del_event(epollfd,fd,EPOLLIN) ;
	}
	else if(0== nread)
	{
		printf("client is closed : %d",fd) ;
		close(fd) ;
		del_event(epollfd,fd,EPOLLIN) ;
	}
	else{
		printf("read message is :%s\n",buf) ;
		modify_event(epollfd,fd,EPOLLOUT) ;
	}
}
void handle_accept(int epollfd,int listenfd)
{
	int clientfd = -1 ;
	struct sockaddr_in clientaddr ;
	socklen_t clientaddr_len = sizeof(clientaddr) ;
	clientfd = accept(listenfd,(struct sockaddr*)&clientaddr,&clientaddr_len) ;
	if(clientfd == -1)
	{
		printf("accept is error : %d\n",errno) ;
	}
	else
	{
		printf("accept a new client: %s : %d\n",inet_ntoa(clientaddr.sin_addr),ntohs(clientaddr.sin_port)) ;
		add_event(epollfd,clientfd,EPOLLIN) ;
	}
}



