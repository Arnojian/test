
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<sys/types.h>
#include<sys/epoll.h>

#include<errno.h>

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void do_write(int epollfd,int fd,int connfd,char *buf) ;
void do_read(int epollfd,int fd,int connfd,char*buf) ;
void add_event(int epollfd,int fd,int state) ;
void modify_event(int epollfd,int fd,int state) ;
void del_event(int epollfd,int fd,int state) ;
void handle_event(int epollfd,struct epoll_event *events,int num,int connfd,char *buf) ;
void do_epoll(int connfd) ;
int main()
{
	int connfd =-1;
	struct sockaddr_in servaddr ;
	connfd = socket(AF_INET ,SOCK_STREAM,0) ;
	bzero(&servaddr,sizeof(servaddr)) ;
	servaddr.sin_family = AF_INET ;
	inet_pton(AF_INET,"127.0.0.1",&servaddr.sin_addr) ;
	servaddr.sin_port = htons(9090) ;

	if(-1==connect(connfd,(struct sockaddr*)&servaddr,sizeof(servaddr)))
	{
		printf("connect is error :%d\n",errno) ;
		exit(1) ;
	}

	do_epoll(connfd) ;
	return 0 ;
}
void do_epoll(int connfd)
{
	int epollfd ;
	struct epoll_event events[1024] ;
	char buf[1024] ;
	int ret ;
	epollfd = epoll_create(5) ;
	add_event(epollfd,STDIN_FILENO,EPOLLIN) ;

	for(;;)
	{
		ret = epoll_wait(epollfd,events,1024,-1) ;
		handle_event(epollfd,events,ret,connfd,buf) ;
	}
	close(epollfd) ;
}

void handle_event(int epollfd,struct epoll_event *events,int num,int connfd,char *buf)
{
	int i = 0 ;
	int clientfd =-1 ;
	for(i = 0 ;i< num;i++)
	{
		clientfd = events[i].data.fd ;
		if(events[i].events & EPOLLIN)
		{
			do_read(epollfd,clientfd,connfd,buf) ;
		}
		else if (events[i].events & EPOLLOUT)
		{
			do_write(epollfd,clientfd,connfd,buf) ;
		}
	}
}


void do_read(int epollfd,int clientfd,int connfd,char *buf)
{
	int nread  ;
	nread = read(clientfd,buf,1024) ;
	if(-1 == nread)
	{
		printf("read is error:%d\n",errno) ;
		close(clientfd) ;
	}
	else if(0==nread)
	{
		printf("server is closed %d\n",errno) ;
		close(clientfd) ;
	}
	else
	{
		if(clientfd == STDIN_FILENO)
		{
			add_event(epollfd,connfd,EPOLLOUT) ;
		}
		else
		{
			del_event(epollfd,connfd,EPOLLIN) ;
			add_event(epollfd,STDOUT_FILENO,EPOLLOUT) ;
		}
	}
}

void do_write(int epollfd,int clientfd,int connfd,char *buf)
{
	int nwrite ;
	nwrite = write(clientfd,buf,strlen(buf)) ;
	if(-1==nwrite)
	{
		printf("write is error") ;
		close(clientfd) ;
	}
	else
	{
		if(clientfd == STDOUT_FILENO)
		{
			del_event(epollfd,clientfd,EPOLLOUT) ;
		}
		else
		{
			modify_event(epollfd,clientfd,EPOLLIN) ;
		}
	}
	memset(buf,0,1024) ;
}

void add_event(int epollfd,int fd,int state)
{
	struct epoll_event ev ;
	ev.data.fd = fd ;
	ev.events = state ;
	epoll_ctl(epollfd,EPOLL_CTL_ADD,fd,&ev) ;
}

void del_event(int epollfd,int fd,int state)
{
	struct epoll_event ev ;
	ev.data.fd = fd ;
	ev.events = state ;
	epoll_ctl(epollfd,EPOLL_CTL_DEL,fd,&ev) ;
}

void modify_event(int epollfd,int fd,int state)
{
	struct epoll_event ev ;
	ev.data.fd = fd ;
	ev.events = state ;
	epoll_ctl(epollfd,EPOLL_CTL_MOD,fd,&ev) ;
}






