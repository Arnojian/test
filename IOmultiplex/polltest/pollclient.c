#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<unistd.h>
#include<netinet/in.h>
#include<poll.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>

void handle_connection(int sockfd)
{
	char sendline[1024],recvline[1024] ;

	struct pollfd pfds[2] ;
	int n ;

	pfds[0].fd = sockfd ;
	pfds[0].events = POLLIN ;

	pfds[1].fd = STDIN_FILENO ;
	pfds[1].events = POLLIN ;

	while(1)
	{
		poll(pfds,2,-1) ;
		if(pfds[0].revents & POLLIN)
		{
			n = read(sockfd,recvline,1024);
			if(n ==0)
			{
				printf("client: server is closed\n") ;
				close(sockfd) ;
			}
			write(STDOUT_FILENO,recvline,n) ;
			//pfds[0].revents &= POLLOUT ;
		}

		if(pfds[1].revents&POLLIN)
		{
				n = read(STDIN_FILENO,sendline,1024) ;
				if( 0 == n )
				{
					shutdown(sockfd,SHUT_WR) ;
					continue ;
				}
				write(sockfd,sendline,n) ;
		}
	}
}

int do_main()
{
	int sockfd ;
	struct sockaddr_in servaddr ;
	sockfd = socket(AF_INET,SOCK_STREAM,0) ;
	bzero(&servaddr,sizeof(servaddr) ) ;

	servaddr.sin_family = AF_INET ;
	servaddr.sin_port = htons(9090) ;
	inet_pton(AF_INET,"127.0.0.1",&servaddr.sin_addr) ;

	connect(sockfd,(struct sockaddr*)&servaddr,sizeof(servaddr)) ;

	handle_connection(sockfd) ;
	return 0 ;
}
int main()
{
	/*
	int connfd ;
	struct sockaddr_in servaddr ;
	int nread = 0 ;
	char buff[1024] ;
	memset(buff,0,1024) ;
	memset(&servaddr,0,sizeof(servaddr)) ;

	servaddr.sin_family = AF_INET ;
	inet_aton("127.0.0.1",&servaddr.sin_addr) ;
	servaddr.sin_port = htons(9090) ;

	connfd = socket(AF_INET,SOCK_STREAM,0) ;
	if(connfd == -1)
	{
		printf("create socket is error:%d",errno) ;
		exit(1) ;
	}

	if(connect(connfd,(struct sockaddr*)&servaddr,sizeof(servaddr))==-1)
	{
		printf("connect is error:%d",errno) ;
		exit(1) ;
	}
	printf("connected") ;
	strcpy(buff,"luojian love liya!") ;
	if(write(connfd,buff,strlen(buff))<0)
	{
		printf("write is error:%d",errno) ;
		exit(1) ;
	}
	printf("write is over") ;
	if((nread = read(connfd,buff,1024))<0)
	{
		printf("read is error%d",errno) ;
		exit(1) ;
	}
	else if(0==nread)
	{
		printf("read nothing") ;
	}
	else
		printf("the buffer is %s\n",buff) ;
	close(connfd) ;
	*/
	do_main() ;
	return 0 ;
}
