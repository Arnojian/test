#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

#include<arpa/inet.h>   //IP地址转换函数
#include<sys/socket.h>  //socket函数及其操作函数
#include<netinet/in.h>	//sockaddr sockaddr_in结构体
#include<poll.h>	//poll有关的函数和结构体
#include<unistd.h> //常用的open write read close函数
#include<sys/types.h> //常用的类型

#define IPADDRESS "127.0.0.1"
#define PORT		9090 ;
#define MAXLINE		1024
#define LISTENQ		5
#define	OPEN_MAX	1000
#define	INFTIM		-1


//创建套接字
int socket_bind(const char *ip,int port)
{
	int listenfd =-1 ;
	struct sockaddr_in servaddr ;

	listenfd = socket(AF_INET,SOCK_STREAM,0) ;
	bzero(&servaddr,sizeof(servaddr)) ;
	servaddr.sin_family = AF_INET ;
	//inet_pton  字符串点十进制转换成二进制
	if(inet_pton(AF_INET,ip,&servaddr.sin_addr)==-1)
	{
		printf("inet_pton is error : %d\n",errno) ;
		exit(1) ;
	}
	servaddr.sin_port= htons(port) ;
	if(bind(listenfd,(struct sockaddr*)&servaddr,sizeof(servaddr)) ==-1)
	{
		printf("bind is error: %d\n",errno) ;
		exit(1) ;
	}
	if(listen(listenfd,LISTENQ)==-1)
	{
		printf("listen is error:%d",errno) ;
		exit(1) ;
	}
	return listenfd ;
}
//处理多个连接
void handle_connection(struct pollfd *connfds,int num)
{
	int i , n ;
	char buff[MAXLINE] ;
	memset(buff,0,MAXLINE) ;

	for(i =1 ;i<num;i++)
	{
		if(connfds[i].fd < 0)
			continue ;
		if(connfds[i].revents & POLLIN)
		{
			n = read(connfds[i].fd,buff,MAXLINE) ;
			if(0==n)
			{
				close(connfds[i].fd) ;
				connfds[i].fd = -1 ;
				continue ;
			}
			printf("%s",buff) ;
			if(write(connfds[i].fd,buff,n) <0)
			{

				printf("the connfds%d write data is error:%d\n",connfds[i].fd,errno) ;
				exit(1) ;
			}
		}
	}

}
//IO多路复用poll
void do_poll(int listenfd)
{
	int connfd ;
	struct sockaddr_in cliaddr ;
	socklen_t	cliaddrlen ;
	struct pollfd	clientfds[OPEN_MAX] ;
	int maxi =-1 ;
	int i ;
	int nready ;
	//clientfds初始化过程
	clientfds[0].fd = listenfd ;
	clientfds[0].events = POLLIN ;
	for(i = 1 ;i< OPEN_MAX;i++)
	{
		clientfds[i].fd = -1 ;
	}
	maxi = 0 ;
	// poll的轮询
	for(;;)
	{
		//无限等待直到有消息返回
		nready = poll(clientfds,maxi+1,INFTIM) ;
		if(nready == -1)
		{
			printf("poll error : %d\n",errno) ;
			exit(-1) ;
		}
		if(clientfds[0].revents & POLLIN)
		{
			cliaddrlen = sizeof(cliaddrlen) ;
			if((connfd = accept(listenfd,(struct sockaddr*)&cliaddr,&cliaddrlen)) ==-1 )
			{
				if(errno == EINTR)
					continue ;
				else
				{
					printf("accept error:%d\n",errno) ;
					exit(1) ;
				}
			}
			fprintf(stdout,"accept a new client: %s:%d\n",inet_ntoa(cliaddr.sin_addr),ntohs(cliaddr.sin_port)) ;

			for(i = 1 ;i< OPEN_MAX;i++)
			{
				if(clientfds[i].fd < 0)
				{
					clientfds[i].fd = connfd ;
					clientfds[i].events=POLLIN ;
						break ;
				}
			}
			maxi = (i>maxi)? i: maxi ;
			if(--nready <= 0 )
				continue ;
		}
		handle_connection(clientfds,maxi+1) ;
	}
}


int main(int argc,char *argv[])
{
	int listenfd ;
	listenfd = socket_bind("127.0.0.1",9090) ;
	do_poll(listenfd) ;
	return 0 ;
}
