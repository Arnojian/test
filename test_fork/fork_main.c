/* 标准输出到终端设备属于行缓冲；
 * 非终端设备属于全缓冲
 * */

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int g_var = 6 ;
char buf[] = "a write to stdout\n" ;

int main()
{
	int var ;
	pid_t pid ;
	var = 88 ;

	if(write(STDOUT_FILENO,buf,sizeof(buf)-1) != sizeof(buf)-1)
	{
		printf(" write error\n") ;
		return 1 ;
	}

	printf("before fork\n") ;

	if((pid = fork())<0)
	{
		printf("fork error\n") ;
		return 1 ;
	}
	else if(pid==0)
	{
		g_var++ ;
		var ++ ;
	}
	else
	{
		sleep(2) ;
	}

	printf("pid=%d,g_var=%d, var=%d\n",getpid(),g_var,var) ;
	return 0 ;
}
