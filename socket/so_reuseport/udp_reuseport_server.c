#include<iostream>
#include<sys/types.h>
#include<string.h>
#include<unistd.h>
#include<stdlib.h>
#include<errno.h>
#include<stdio.h>


#include<netinet/in.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<string.h>
#include<pthread.h>
#include<sys/syscall.h>


using namespace std;

#define BUFFER_SIZE 1024

string gIp ;
unsigned short gPort ;

pid_t gettid()
{
	return syscall(SYS_gettid) ;
}


inline string IpU32ToString(unsigned int ipv4)
{
	char buf[BUFFER_SIZE] ={0} ;
	struct in_addr in ;
	in.s_addr = ipv4 ;
	
	if(inet_ntop(AF_INET,&in,buf,sizeof(buf)))
	{
		return string(buf) ;
	}
	else
	{
		return string("") ;
	}
}

void startUdpServer(string &ip, unsigned short port)
{
	cout<<"ip:"<<ip<<" port:"<<port<<" "<<gettid()<<endl ;
	
	struct sockaddr_in servAddr ;
	socklen_t	servAddrLen = sizeof(servAddr) ;
	
	memset(&servAddr,0,servAddrLen) ;
	servAddr.sin_family = AF_INET ;
	servAddr.sin_addr.s_addr = inet_addr(ip.c_str()) ;
	servAddr.sin_port = htons(port) ;

	int servFd = socket(AF_INET,SOCK_DGRAM,0) ;
	if(servFd <0)
	{
		perror("create socket is error!") ;
		exit(1) ;
	}

	int l_optVal =1 ;
	setsockopt(servFd,SOL_SOCKET,SO_REUSEPORT,&l_optVal,sizeof(l_optVal));
	setsockopt(servFd,SOL_SOCKET,SO_REUSEADDR,&l_optVal,sizeof(l_optVal));

	if ( -1 == bind(servFd,(struct sockaddr*)&servAddr,servAddrLen))
	{
		perror("bind is error") ;
		exit(1) ;
	}
		
	while(1)
	{
		struct sockaddr_in l_cliAddr ;
		socklen_t l_cliAddrLen = sizeof(l_cliAddr) ;

		char buff[BUFFER_SIZE] ;
		bzero(buff,BUFFER_SIZE) ;
	
		cout<<"begin to recv data..."<< " "<<gettid()<<endl ;

		int ret = recvfrom(servFd,buff,BUFFER_SIZE,0,(struct sockaddr*)&l_cliAddr,&l_cliAddrLen) ;
		if( -1 == ret)
		{
			perror("receive data failed") ;
			exit(1) ;
		}

		cout<<"recv "<<ret<<" bytes from "<<IpU32ToString(l_cliAddr.sin_addr.s_addr)<<" : "<< l_cliAddr.sin_port<<gettid()<<endl ;
		
		ret = sendto(servFd,buff,ret,0,(struct sockaddr*)&l_cliAddr,sizeof(l_cliAddr)) ;
		if(ret < 0)
		{
			perror("error sendto") ;
			exit(1) ;
		}
		cout<<"response "<<ret << " bytes to client."<< " "<<gettid()<<endl ;
	}

	pthread_exit(0) ;

}

void * threadFunc(void *arg)
{
	startUdpServer(gIp,gPort) ;
}

int main(int argc,char **argv)
{
	if(argc < 4)
	{
		cout<<"usage:"<<argv[0]<<"<local_ip><udp_port><thread_count>"<<endl;
		exit(0) ;
	}
	gIp = argv[1] ;
	gPort= atoi(argv[2]) ;
	int threadCount = atoi(argv[3]) ;
	cout<<"ip:"<<gIp<<" port:"<<gPort<<" threadCount:"<<threadCount<<endl;

	pthread_t threadFd[threadCount] ;
	for(int i = 0 ;i<threadCount; i++)
	{
		pthread_create(&threadFd[i],NULL,&threadFunc,NULL) ;
	}

	for(int i = 0 ;i<threadCount;i++)
	{
		pthread_join(threadFd[i],NULL) ;
	}
	
	return 0 ;
}








