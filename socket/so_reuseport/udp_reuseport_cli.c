#include<iostream>
#include<sys/types.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>

#include<netinet/in.h>
#include<sys/socket.h>
#include<arpa/inet.h>

#define BUFFER_SIZE 1024

using namespace std;
string gIp ;
unsigned short gPort ;

inline string IpU32ToString(unsigned int ipv4)
{
	struct in_addr in ;
	char buff[BUFFER_SIZE] ;
	in.s_addr = ipv4 ;

	if(inet_ntop(AF_INET,&in,buff,BUFFER_SIZE))
	{
		return string(buff) ;
	}
	else
	{
		return string("") ;
	}
}

int main(int argc,char **argv)
{
	if(argc <3)
	{
		cout<<"usage : "<<argv[0]<<" <remoteIp><remotePort>"<<endl;
		exit(0);
	}
	gIp = argv[1] ;
	gPort = atoi(argv[2]) ;
	
	cout<<"serverIp:"<<gIp<<" port:"<<gPort<<endl;

	struct sockaddr_in servAddr,cliAddr ;
	socklen_t cliAddrLen = sizeof(servAddr) ;

	bzero(&servAddr,sizeof(cliAddrLen));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr= inet_addr(gIp.c_str());
	servAddr.sin_port=htons(gPort);

	int cliFd = socket(AF_INET,SOCK_DGRAM,0);
	if(cliFd < 0)
	{
		perror("create socket failed");
		exit(1);
	}
	
	char buffer[BUFFER_SIZE]="iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiend" ;
	int len = BUFFER_SIZE ;
	int i = 0 ;
	int max = 10 ;
	cout<<"please input the number of sending pkg"<<endl;
	cin>>max ;
	while(i++ < max)
	{
		cout<<"send to "<<IpU32ToString(servAddr.sin_addr.s_addr)<<" port "<<ntohs(servAddr.sin_port)<<endl;

		int ret = sendto(cliFd,buffer,strlen(buffer)+1,0,(struct sockaddr*)&servAddr,sizeof(servAddr));
		if(ret <0)
		{
			perror("send to relay fail:");
			exit(1) ;
		}
		
		cout<< "send "<<ret <<"bytes to relay server "<<IpU32ToString(servAddr.sin_addr.s_addr)<<" "<<ntohs(servAddr.sin_port)<<endl;

		ret = recvfrom(cliFd,buffer,BUFFER_SIZE,0,(struct sockaddr*)&cliAddr,&cliAddrLen);
		if(ret <0)
		{
			perror("receive data failed");
			exit(1) ;
		}
		cout<<"recv from "<<IpU32ToString(cliAddr.sin_addr.s_addr)<<" "<<ntohs(cliAddr.sin_port)<<" data Len:"<<ret<<endl;

	}
}




