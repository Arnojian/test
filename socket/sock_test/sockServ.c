#include<stdio.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<errno.h>
#include<string.h>
#include<netinet/in.h>


#define IP	"127.0.0.1"
#define PORT 4444

/*
	functionName: createSocket
	description:create and bind the socket ;
				set the recv/send buffer ;
	input		: ip, port
	output		: socket/ -1(create failed) 
*/
int createSocket(char *ip,int port)
{
	int sockfd = -1;
	sockfd = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP) ;
	if (sockfd < 0)
	{
		printf("create socket is error:%s\n",strerror(errno));
		return -1 ;
	}

	struct sockaddr_in servAddr ;
	memset(&servAddr,0,sizeof(servAddr)) ;
	servAddr.sin_family = AF_INET ;
	servAddr.sin_port = htons(port) ;
	servAddr.sin_addr.s_addr = inet_addr(ip) ;

	int ret = bind(sockfd,(struct sockaddr *)&servAddr, sizeof(servAddr) ) ;
	if (ret < 0)
	{
		printf("LINE:%d,bind socket is error :%s\n",__LINE__,strerror(errno)) ;
		return -1 ;
	}

	int vRecvBuf = 10 ; //20�ֽ�
	ret = setsockopt(sockfd,SOL_SOCKET,SO_RCVBUF,(const char *)&vRecvBuf,sizeof(vRecvBuf)) ;
	if (ret < 0)
	{
		printf("LINE:%d,setsockopt RCVBUF is error:%s\n",__LINE__,strerror(errno)) ;
		return -1 ;
	}

	ret = setsockopt(sockfd,SOL_SOCKET,SO_SNDBUF,(const char *)&vRecvBuf,sizeof(vRecvBuf)) ;
	if (ret < 0)
	{
		printf("LINE:%d,setsockopt RCVBUF is error:%s\n",__LINE__,strerror(errno)) ;
		return -1 ;
	}

	ret = listen(sockfd,5) ;
	if (ret <0)
	{
		printf("listen is error:%s\n",strerror(errno)) ;
		return -1 ;
	}
	printf("LINE:%d,create socket is success\n",__LINE__) ;
	return sockfd ;
}

#define buffLen  1024
char buff[buffLen] ;
int pos = 0 ;

int dealMsg(int *cliSock)
{
	int nread = recv(*cliSock,buff+pos,1,0) ;
	if (nread < 0)
	{
		if((EAGAIN==errno) || (EWOULDBLOCK==errno) || (EINTR==errno) )
		{
			printf("no error:%s\n",strerror(errno)) ;
			return 0 ;
		}
		else
		{
			close(*cliSock) ;
			*cliSock = -1 ;
			return -1 ;
		}
	}
	else if(nread ==0)
	{
		close(*cliSock) ;
		*cliSock = -1 ;
		return -1 ;
	}
	else
	{
		printf("LINE:%d,recvDataTotal:%d,nread:%d \n",__LINE__,pos,nread) ;
		if (pos >= buffLen)
		{
			pos = 0 ;
		}
		pos += nread ;
		sleep(1) ;
	}
	return 0 ;
}


int main()
{
	int servSock = -1 ;
	struct sockaddr_in cliAddr ;
	socklen_t	cliAddrLen = sizeof(cliAddr) ;
	servSock = createSocket(IP,PORT) ;
	if (servSock == -1)
	{
		printf("LINE:%d create socket is failed\n",__LINE__) ;
		return -1 ;
	}

	fd_set readFd ;
	int ret = -1 ;
	int cliSock = 0 ;
	int max  = 0 ;
	while(1)
	{
		
		FD_ZERO(&readFd) ;
		FD_SET(servSock,&readFd) ;
		max = servSock ;
		if (cliSock > 0)
		{
			FD_SET(cliSock,&readFd) ;
			max = cliSock>servSock? cliSock:servSock ;
		}

		printf("max=%d\n",max) ;
		ret = select(max+1,&readFd,NULL,NULL,NULL) ;
		if (ret <= 0)
		{	
			if (cliSock >0)
			{
				close(cliSock) ;
			}
			close(servSock) ;
			return -1 ;
		}
		else // some one is recv
		{
			printf("ret == %d\n",ret) ;
			while(ret)
			{
				if (FD_ISSET(servSock,&readFd))
				{
					cliSock = accept(servSock,(struct sockaddr*)&cliAddr,&cliAddrLen) ;
					if (cliSock <0)
					{
						printf("LINE:%d,create client socket is error\n",__LINE__) ;
						close(cliSock) ;
						cliSock = -1 ;
						break ;
					}
				}
				else if (FD_ISSET(cliSock,&readFd))
				{
					dealMsg(&cliSock) ;
				}
				ret --;
			}
		}
	}

	return 0 ;
}