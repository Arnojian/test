#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<errno.h>
#include<arpa/inet.h> //htons,inet_addr

#include<netinet/in.h>
#include<string.h>

#define PORT 4444
#define IP	"127.0.0.1"
#define LEN  1024*100
char buff[LEN] ;

int main()
{
	struct sockaddr_in servAddr;
	int servSock = -1;
	socklen_t	servSockLen = sizeof(servAddr) ;
	
	
	servSock = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP) ;
	if (servSock < 0)
	{
		printf("LINE:%d,create socket is error:%s\n",__LINE__,strerror(errno)) ;
	}

	memset(&servAddr,0,sizeof(servAddr)) ;
	servAddr.sin_family = AF_INET ;
	servAddr.sin_port = htons(PORT) ;
	servAddr.sin_addr.s_addr = inet_addr(IP) ;


	int vRecvBuf = 10 ; //20�ֽ�
	int ret = setsockopt(servSock,SOL_SOCKET,SO_RCVBUF,(const char *)&vRecvBuf,sizeof(vRecvBuf)) ;
	if (ret < 0)
	{
		printf("LINE:%d,setsockopt RCVBUF is error:%s\n",__LINE__,strerror(errno)) ;
		return -1 ;
	}

	ret = setsockopt(servSock,SOL_SOCKET,SO_SNDBUF,(const char *)&vRecvBuf,sizeof(vRecvBuf)) ;
	if (ret < 0)
	{
		printf("LINE:%d,setsockopt RCVBUF is error:%s\n",__LINE__,strerror(errno)) ;
		return -1 ;
	}

	 ret = connect(servSock,(struct sockaddr*)&servAddr,servSockLen) ;
	if (ret <0)
	{
		printf("LINE:%d,connect is error:%s\n",__LINE__,strerror(errno)) ;
		return -1 ;
	}
	


	printf("connect is success\n") ;


	memset(buff,'1',LEN) ;
	buff[LEN] = '\0' ;
	int left = LEN ;
	int pos  = 0 ;
	int nwrite = 0 ;
	int flag = 0 ;
	int sendLen = 1024 ;

	while(left >0)
	{
		nwrite = send(servSock,buff+pos,sendLen,0) ;
		if (nwrite < 0)
		{
			if (errno==EINTR || errno== EWOULDBLOCK || errno == EAGAIN)
			{
				printf("err:%s\n",strerror(errno));
				continue ;
			}
			printf("send is error:%s\n",strerror(errno)) ;
			close(servSock) ;
			return -1 ;
		}
		else
		{
			left -= nwrite ;
			pos += nwrite ;
			printf("send dataLen :%d\n",pos) ;
			if(flag ==0)
			{
				sendLen = 1024;
				flag =1 ;
			}
			
		}
	}
	

	printf("send all:%d\n",pos) ;

	return 0 ;
}
