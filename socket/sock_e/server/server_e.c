 #include"server_e.h"
 int main()
 {
  int sock =-1;
  int new_sock=-1;
	int on = 1 ;
	int res ;
	int buffsize = 1024*1024*4 ;
	struct sockaddr_in svraddr ;
	struct sockaddr_in localaddr ;
	int var ;
	sock = socket(AF_INET,SOCK_STREAM,0) ;
	if(sock == -1)
	{
		printf("socket error\n") ;
		return -1 ;
	}
	
		svraddr.sin_family =AF_INET ;
		svraddr.sin_port = htons(9999) ;
		svraddr.sin_addr.s_addr  = inet_addr("127.0.0.1") ;
	
	if(bind(sock,(const struct sockaddr*)&svraddr,sizeof(svraddr))<0)
	{
			printf("bind is error %d\n",errno) ;
			close(sock) ;
			return -1 ;
	}
		
		if(setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,(void*)&on,sizeof(on))==-1) 
		{
					printf("sockopt is error\n") ;
					close(sock) ;
					return -1 ;
		}
		//设置接收buff大小
		if(setsockopt(sock,SOL_SOCKET,SO_RCVBUF,&buffsize,sizeof(buffsize))==-1)
		{
				printf("sockopt is error\n") ;
				close(sock) ;
				return -1 ;
		}
		
		//设置发送buff大小
		if(setsockopt(sock,SOL_SOCKET,SO_SNDBUF,&buffsize,sizeof(buffsize))==-1)
		{
				printf("sockopt is error\n") ;
				close(sock) ;
				return -1 ;
		}
		
		var = fcntl(sock,F_GETFL);
		if(var < 0)
		{
			printf("Cannot get socket flag!");
			close(sock) ;
			return -1 ;
		}
		var |=O_NONBLOCK;
		if(fcntl(sock,F_SETFL,var)<0) 
		{
				printf("Cannot set socket flag!\n") ;
				close(sock) ;
				return -1 ;
		}
		
		listen(sock,5) ;
		res = sizeof(localaddr) ;
		while( new_sock <0)
		{
			new_sock = accept(sock,(struct sockaddr*)&localaddr,(socklen_t*)&res) ;
		
		}
		printf("%s %d\n",inet_ntoa(localaddr.sin_addr),ntohs(localaddr.sin_port)) ;
		return 0 ;
	}
