#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>


int system_1(const char* cmdString)
{
	
	pid_t pid ;
	int status ;
	
	if( NULL == cmdString)
	{
		return 1 ;
	}

	if( (pid = fork())<0)
	{
		status = -1 ;
	}
	else if(0==pid)
	{
		execl("/bin/sh","sh","-c",cmdString,(char *)0) ;
		_exit(127) ;
	}
	else
	{
		while(waitpid(pid,&status,0)<0)
		{
			if(EINTR!=errno)
			{
				status = -1 ;
				break ;
			}
		}
	}
	
	return status ;
}

int main()
{
	int status ;
	
	if( (status = system_1("date"))<0)
	{
		printf("system error date\n") ;
	}
	printf("date exit status :%d\n",status) ;

	if((status = system_1("nosuchcommand"))<0)
	{
		printf("system error nosuchcommand\n") ;
	}
	printf("nosuchcommand status :%d\n",status) ;

	if((status = system_1("who;exit 44"))<0)
	{
		printf("who ;exit 44\n") ;
	}
	printf("status who;exit 44 :%d\n",status) ;

	return 0 ;
}
