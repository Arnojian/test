#include"select_e.h"
int main()
{
	int keyboard ;
	int ret ;
	fd_set readfds ;
	char key ;
	struct timeval timeout ;
	char *path = "/dev/tty" ;
	keyboard = open(path,O_RDONLY | O_NONBLOCK) ;
	if(keyboard  < 0 )
		{
			printf("open error!\n") ;
			return 1 ;
		}	
	printf("keyboard is %d\n",keyboard) ;
	timeout.tv_sec = 0 ;
	timeout.tv_usec = 0 ;
	while(1)
	{
		FD_ZERO(&readfds) ;
		FD_SET(keyboard,&readfds) ;
		ret = select(keyboard+1,&readfds,NULL,NULL,&timeout) ;
		if(ret < 0)
		{
			printf("select error!\n") ;
			return 1 ;	
		}
		ret = FD_ISSET(keyboard,&readfds) ;
		if(ret > 0 )
			{
				read(keyboard,&key,1) ;
				if('\n' == key)
					continue ;
				printf("the input is %c \n",key) ;
				if('q' == key)
					break ;
			}
	}
	return 0 ;
}
