#include <errno.h>
#include <stdarg.h>
#include <string.h>

static void err_doit(int,int,const char *,va_list) ;

void err_ret(const char* fmt,...)
{
	va_list ap ;
	va_start(ap,fmt) ;
	err_doit(1,errno,fmt,ap) ;
	va_end(ap) ;
}

void err_sys(const char *fmt,...)
{
	va_list ap ;

	va_start(ap,fmt) ;
	err_doit(1,errno,fmt,ap) ;
	va_end(ap);
	exit(1) ;
}

void err_exit(int error,const char *fmt,...)
{
	va_list ap ;
	
	va_start(ap,fmt) ;
	err_doit(1,error,fmt,ap);
	va_end(ap);
	exit(1) ;
}


void err_dump(const char *fmt,...)
{
	va_list ap ;

	va_start(ap,fmt) ;
	err_doit(1,errno,fmt,ap);
	va_end(ap) ;
	abort() ;
	exit(1) ;
}

void err_msg(const char *fmt,...)
{
	va_list ap ;

	va_start(ap,fmt) ;
	err_doit(0,0,fmt,ap) ;
	va_end(ap) ;
}


void err_qiut(const char *fmt,...)
{
	va_list(ap) ;

	va_start(ap,fmt) ;
	err_doit(0,0,fmt,ap) ;
	va_end(ap) ;
	exit(1) ;
}

static void err_doit(int errnoFlag,int error,const char *fmt,va_list ap)
{
	char buf[128] ;
	vsnprintf(buf,128,fmt,ap) ;
	if(errnoFlag)
		snprintf(buf+strlen(buf),128-strlen(buf),":%s",strerror(error)) ;

	strcat(buf,"\n") ;
	fflush(stdout) ;
	fputs(buf,stderr) ;
	fflush(NULL) ;
}




















