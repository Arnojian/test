#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include "my_err.h"

#define MAX_BUFFER 128
static void sig_int(int) ;

int main(void)
{
	char buf[MAX_BUFFER] ;
	pid_t pid ;
	int status ;

	if(signal(SIGINT,sig_int) == SIG_ERR)
	{
		err_sys("signal error") ;
		return 1 ;
	}

	while(fgets(buf,sizeof(buf),stdin) != NULL)
	{
		buf[strlen(buf)-1] = '\0' ;

		if( pid = fork() < 0)
		{
			err_sys("fork error") ;
		}
		else if(pid ==0)
		{
			printf("the child pid\n") ;
			exit(127) ;
		}

		if((pid == waitpid(pid,&status,0)) <0)
		{
			err_sys("waitpid error") ;
		}
	}
	return 0 ;
}
void sig_int(int signo)
{
	printf("interrupt\n") ;
}
