#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

char buf[1024] = {0};
int glen = 0;

typedef struct
{	
	uint8_t type;
	uint8_t len;
	char*   value;
}tlv;


void addtlv(uint8_t type, uint8_t len, char* value)
{
	char* ptr = buf + glen;
	*(uint8_t*)ptr = type;
	glen += sizeof(uint8_t);

	ptr = buf + glen;
	*(uint8_t*)ptr = len;
	glen += sizeof(uint8_t);

	ptr = buf + glen;
	int tmplen = strlen(value) + 1;
	memcpy(ptr, value, tmplen);
	glen +=tmplen;
	return;
}

void gettlv(char** preptr, uint8_t* prelen, tlv* ptlv)
{
	char *ptr = *preptr;
	uint8_t len = *prelen;

	ptlv->type = *(uint8_t*)ptr;

	ptr += sizeof(uint8_t);

	ptlv->len = *(uint8_t*)ptr;
    len -= ptlv->len;
	ptr += sizeof(uint8_t);

	//strcpy(ptlv->value, ptr);
	ptlv->value = ptr;

	ptr += sizeof(ptlv->len)-2;

	*preptr = ptr;
	*prelen = len;

	return;
}

int main()
{
	char *value = "hello world!";
	int length = 2+strlen(value)+1 ;
	glen = 0;
	addtlv(1,length,value);
	addtlv(1,length,value);
	addtlv(1,length,value);
	

	char* ptr = buf;
	uint8_t len = glen;

	tlv tlv1;
	memset((char*)&tlv1, 0, sizeof(tlv));
	gettlv(&ptr, &len, &tlv1);
	printf("tlv1 type:%u,len:%u,value:%s\n", tlv1.type, tlv1.len, tlv1.value);

	tlv tlv2;
	memset((char*)&tlv2, 0, sizeof(tlv));
	gettlv(&ptr, &len, &tlv2);
	printf("tlv1 type:%u,len:%u,value:%s\n", tlv2.type, tlv2.len, tlv2.value);


	tlv tlv3;
	memset((char*)&tlv3, 0, sizeof(tlv));
	gettlv(&ptr, &len, &tlv3);
	printf("tlv1 type:%u,len:%u,value:%s\n", tlv3.type, tlv3.len, tlv3.value);

	return 0;
}
